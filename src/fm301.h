/*---------------------------------------------------------------------------------------------------------------------
 * FM301 C++ API and Toolkit
 *
 * Copyright 2021 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *-------------------------------------------------------------------------------------------------------------------*/
#pragma once

#include <chrono>
#include <cmath>
#include <filesystem>
#include <optional>
#include <span>
#include <string>
#include <variant>
#include <vector>

namespace fm301
{
  /// WMO standard quantities
  enum class standard_quantity
  {
      dbzh  ///< Equivalent reflectivity factor H
    , dbzv  ///< Equivalent reflectivity factor V
    , zh    ///< Linear equivalent reflectivity factor H
    , zv    ///< Linear equivalent reflectivity factor V
    , dbth  ///< Total power H (uncorrected reflectivity)
    , dbtv  ///< Total power V (uncorrected reflectivity)
    , th    ///< Linear total power H (uncorrected reflectivity)
    , tv    ///< Linear total power V (uncorrected reflectivity)
    , vradh ///< Radial velocity of scatterers away from instrument H
    , vradv ///< Radial velocity of scatterers away from instrument V
    , wradh ///< Doppler spectrum width H
    , wradv ///< Doppler spectrum width V
    , zdr   ///< Log differential reflectivity H/V
    , ldr   ///< Log-linear depolarization ratio HV
    , ldrh  ///< Log-linear depolarization ratio H
    , ldrv  ///< Log-linear depolarization ratio V
    , phidp ///< Differential phase HV
    , kdp   ///< Specific differential phase HV
    , phihx ///< Cross-polar differential phase
    , rhohv ///< Correlation coefficient HV
    , rhohx ///< Co-to-cross polar correlation coefficient H
    , rhoxv ///< Co-to-cross polar correlation coefficient V
    , dbm   ///< Log power
    , dbmhc ///< Log power co-polar H
    , dbmhx ///< Log power cross-polar H
    , dbmvc ///< Log power co-polar V
    , dbmvx ///< Log power cross-polar V
    , snr   ///< Signal-to-noise ratio
    , snrhc ///< Signal-to-noise ratio co-polar H
    , snrhx ///< Signal-to-noise ratio cross-polar H
    , snrvc ///< Signal-to-noise ratio co-polar V
    , snrvx ///< Signal to noise ratio cross polar V
    , ncp   ///< Normalized coherent power
    , ncph  ///< Normalized coherent power co-polar H
    , ncpv  ///< Normalized coherent power co-polar V
    , rr    ///< Rain rate
    , rec   ///< Radar echo classification
  };

  /// Metadata values associated with a dataset quantity
  struct quantity_traits
  {
    char const* name;           ///< Standard dataset variable name
    char const* standard_name;  ///< Value of CF standard_name attribute
    char const* long_name;      ///< Value of CF long_name attribute
    char const* units;          ///< Value of CF unit attribute
  };

  /// Get the standard metadata values associated with a standard quantity
  auto standard_quantity_traits(standard_quantity quantity) -> quantity_traits const&;

  /// I/O mode for opening a file
  enum class io_mode
  {
      create      ///< Create a new file
    , read_write  ///< Open an existing file for read and write
    , read_only   ///< Open an existing file for read only
  };

  /* The types in data_type, data_type_scalar and data_type_vector must EXACTLY correspond, including their order.
   * This is because we expect the value of data_type to match the index of the corresponding types in the variants. */

  /// Types for dataset encoding
  enum class data_type
  {
      i8
    , u8
    , i16
    , u16
    , i32
    , u32
    , i64
    , u64
    , f32
    , f64
  };

  /// Variant for a scalar in any of the supported data types
  /** This type is used for well known scalar metadata attribute where the type of the attribute must match the type of
   *  the underlying dataset.  Examples include the _Fill_Value and _Undetect. */
  using data_type_scalar = std::variant<
      int8_t
    , uint8_t
    , int16_t
    , uint16_t
    , int32_t
    , uint32_t
    , int64_t
    , uint64_t
    , float
    , double
    >;

  /// Variant for a vector of any of the supported data types
  /** This type is used for well known array based metadata attribute where the type of the attribute must match the
   *  type of the underlying dataset.  Examples include the _Fill_Value and _Undetect. */
  using data_type_vector = std::variant<
      std::vector<int8_t>
    , std::vector<uint8_t>
    , std::vector<int16_t>
    , std::vector<uint16_t>
    , std::vector<int32_t>
    , std::vector<uint32_t>
    , std::vector<int64_t>
    , std::vector<uint64_t>
    , std::vector<float>
    , std::vector<double>
    >;

  /// Variant for a generic metadatum stored as an attribute
  /** This type is used for generic (non-standard) metadata attributes.  Since attributes are not strongly dimensioned
   *  it is possible to store both scalars and 1D arrays in this way. */
  using metadata_value = std::variant<
      int8_t
    , uint8_t
    , int16_t
    , uint16_t
    , int32_t
    , uint32_t
    , int64_t
    , uint64_t
    , float
    , double
    , std::string
    , std::vector<int8_t>
    , std::vector<uint8_t>
    , std::vector<int16_t>
    , std::vector<uint16_t>
    , std::vector<int32_t>
    , std::vector<uint32_t>
    , std::vector<int64_t>
    , std::vector<uint64_t>
    , std::vector<float>
    , std::vector<double>
    , std::vector<std::string>
    >;

  /// Variant for a generic metadatum stored as a scalar variable
  /** This type is used for generic (non-standard) metadata variables.  Since attributes are strongly dimensioned there
   *  are separate functions for storing scalar variables, and 1D variables for each well known dimension.  This type
   *  is used in the case of scalar variables. */
  using metadata_scalar = std::variant<
      int8_t
    , uint8_t
    , int16_t
    , uint16_t
    , int32_t
    , uint32_t
    , int64_t
    , uint64_t
    , float
    , double
    , std::string
    >;

  /// Variant for a generic metadatum stored as a 1D variable
  /** This type is used for scalar generic (non-standard) metadata variables.  Since attributes are strongly dimensioned
   *  there are separate functions for storing scalar variables, and 1D variables for each well known dimension.  This
   *  type is used in the case of 1D variables. */
  using metadata_vector = std::variant<
      std::vector<int8_t>
    , std::vector<uint8_t>
    , std::vector<int16_t>
    , std::vector<uint16_t>
    , std::vector<int32_t>
    , std::vector<uint32_t>
    , std::vector<int64_t>
    , std::vector<uint64_t>
    , std::vector<float>
    , std::vector<double>
    , std::vector<std::string>
    >;

  template <typename T>
  struct is_nan
  {
    auto operator()(T val) const { return std::isnan(val); }
  };

  template <typename T>
  constexpr auto nan = std::numeric_limits<T>::quiet_NaN();

  class dataset;
  class sweep;
  class volume;

  class dataset
  {
  public:
    /// Get dataset name
    /** This is the name of the field variable.  Also known as the 'short name'. */
    auto name() const -> std::string;

    /// Get data type used for encoding
    auto type() const -> data_type;

    /// Check whether an attribute of the dataset variable exists
    auto attribute_exists(char const* name) const -> bool;
    auto attribute_exists(std::string const& name) const -> bool { return attribute_exists(name.c_str()); }

    /// Get an attribute of the dataset variable
    auto attribute_get(char const* name) const -> std::optional<metadata_value>;
    auto attribute_get(std::string const& name) const -> std::optional<metadata_value> { return attribute_get(name.c_str()); }

    /// Set an attribute of the dataset variable
    auto attribute_set(char const* name, metadata_value const& val) -> void;
    auto attribute_set(std::string const& name, metadata_value const& val) -> void { attribute_set(name.c_str(), val); }

    /// Erase an attribute of the dataset variable
    auto attribute_erase(char const* name) -> void;
    auto attribute_erase(std::string const& name) -> void { attribute_erase(name.c_str()); }

    //------------------------------------------------------------------------------------------------------------------

    auto standard_name() const -> std::optional<std::string>;
    auto set_standard_name(std::string const& val) -> void;

    auto long_name() const -> std::optional<std::string>;
    auto set_long_name(std::string const& val) -> void;

    auto units() const -> std::optional<std::string>;
    auto set_units(std::string const& val) -> void;

    auto fill_value() const -> std::optional<data_type_scalar>;
    auto set_fill_value(data_type_scalar val) -> void;

    auto undetect() const -> std::optional<data_type_scalar>;
    auto set_undetect(data_type_scalar val) -> void;

    auto scale_factor() const -> std::optional<float>;
    auto set_scale_factor(float val) -> void;

    auto add_offset() const -> std::optional<float>;
    auto set_add_offset(float val) -> void;

    auto coordinates() const -> std::optional<std::string>;
    auto set_coordinates(std::string const& val) -> void;

    auto sampling_ratio() const -> std::optional<float>;
    auto set_sampling_ratio(float val) -> void;

    auto is_discrete() const -> std::optional<bool>;
    auto set_is_discrete(bool val) -> void;

    auto field_folds() const -> std::optional<bool>;
    auto set_field_folds(bool val) -> void;

    auto fold_limit_lower() const -> std::optional<float>;
    auto set_fold_limit_lower(float val) -> void;

    auto fold_limit_upper() const -> std::optional<float>;
    auto set_fold_limit_upper(float val) -> void;

    auto is_quality_field() const -> std::optional<bool>;
    auto set_is_quality_field(bool val) -> void;

    auto flag_values() const -> std::optional<data_type_vector>;
    auto set_flag_values(data_type_vector const& val) -> void;

    auto flag_meanings() const -> std::optional<std::vector<std::string>>;
    auto set_flag_meanings(std::vector<std::string> const& val) -> void;

    auto flag_masks() const -> std::optional<data_type_vector>;
    auto set_flag_masks(data_type_vector const& val) -> void;

    auto qualified_variables() const -> std::optional<std::vector<std::string>>;
    auto set_qualified_variables(std::vector<std::string> const& val) -> void;

    auto ancillary_variables() const -> std::optional<std::vector<std::string>>;
    auto set_ancillary_variables(std::vector<std::string> const& val) -> void;

    auto thresholding_xml() const -> std::optional<std::string>;
    auto set_thresholding_xml(std::string const& val) -> void;

    auto legend_xml() const -> std::optional<std::string>;
    auto set_legend_xml(std::string const& val) -> void;

    //------------------------------------------------------------------------------------------------------------------

    /// Read data automatically applying any scale_factor/add_offset and replacing _FillValue and _Undetect values
    auto read(std::span<int8_t> data, int8_t fill, int8_t undetect) const -> void { read_impl(data, fill, undetect); }
    auto read(std::span<uint8_t> data, uint8_t fill, uint8_t undetect) const -> void { read_impl(data, fill, undetect); }
    auto read(std::span<int16_t> data, int16_t fill, int16_t undetect) const -> void { read_impl(data, fill, undetect); }
    auto read(std::span<uint16_t> data, uint16_t fill, uint16_t undetect) const -> void { read_impl(data, fill, undetect); }
    auto read(std::span<int32_t> data, int32_t fill, int32_t undetect) const -> void { read_impl(data, fill, undetect); }
    auto read(std::span<uint32_t> data, uint32_t fill, uint32_t undetect) const -> void { read_impl(data, fill, undetect); }
    auto read(std::span<int64_t> data, int64_t fill, int64_t undetect) const -> void { read_impl(data, fill, undetect); }
    auto read(std::span<uint64_t> data, uint64_t fill, uint64_t undetect) const -> void { read_impl(data, fill, undetect); }
    auto read(std::span<float> data, float fill = nan<float>, float undetect = nan<float>) const -> void { read_impl(data, fill, undetect); }
    auto read(std::span<double> data, double fill = nan<double>, double undetect = nan<double>) const -> void { read_impl(data, fill, undetect); }

    /// Write data automatically applying any scale_factor and/or add_offset
    template <class IsFillValue = is_nan<int8_t>, class IsUndetect = is_nan<int8_t>>
    auto write(std::span<int8_t const> data) -> void { write_impl<int8_t, IsFillValue, IsUndetect>(data); }
    template <class IsFillValue = is_nan<uint8_t>, class IsUndetect = is_nan<uint8_t>>
    auto write(std::span<uint8_t const> data) -> void { write_impl<uint8_t, IsFillValue, IsUndetect>(data); }
    template <class IsFillValue = is_nan<int16_t>, class IsUndetect = is_nan<int16_t>>
    auto write(std::span<int16_t const> data) -> void { write_impl<int16_t, IsFillValue, IsUndetect>(data); }
    template <class IsFillValue = is_nan<uint16_t>, class IsUndetect = is_nan<uint16_t>>
    auto write(std::span<uint16_t const> data) -> void { write_impl<uint16_t, IsFillValue, IsUndetect>(data); }
    template <class IsFillValue = is_nan<int32_t>, class IsUndetect = is_nan<int32_t>>
    auto write(std::span<int32_t const> data) -> void { write_impl<int32_t, IsFillValue, IsUndetect>(data); }
    template <class IsFillValue = is_nan<uint32_t>, class IsUndetect = is_nan<uint32_t>>
    auto write(std::span<uint32_t const> data) -> void { write_impl<uint32_t, IsFillValue, IsUndetect>(data); }
    template <class IsFillValue = is_nan<int64_t>, class IsUndetect = is_nan<int64_t>>
    auto write(std::span<int64_t const> data) -> void { write_impl<int64_t, IsFillValue, IsUndetect>(data); }
    template <class IsFillValue = is_nan<uint64_t>, class IsUndetect = is_nan<uint64_t>>
    auto write(std::span<uint64_t const> data) -> void { write_impl<uint64_t, IsFillValue, IsUndetect>(data); }
    template <class IsFillValue = is_nan<float>, class IsUndetect = is_nan<float>>
    auto write(std::span<float const> data) -> void { write_impl<float, IsFillValue, IsUndetect>(data); }
    template <class IsFillValue = is_nan<double>, class IsUndetect = is_nan<double>>
    auto write(std::span<double const> data) -> void { write_impl<double, IsFillValue, IsUndetect>(data); }

    // Read data without applying any scale_factor and/or add_offset
    auto read_raw(std::span<int8_t> data) const -> void;
    auto read_raw(std::span<uint8_t> data) const -> void;
    auto read_raw(std::span<int16_t> data) const -> void;
    auto read_raw(std::span<uint16_t> data) const -> void;
    auto read_raw(std::span<int32_t> data) const -> void;
    auto read_raw(std::span<uint32_t> data) const -> void;
    auto read_raw(std::span<int64_t> data) const -> void;
    auto read_raw(std::span<uint64_t> data) const -> void;
    auto read_raw(std::span<float> data) const -> void;
    auto read_raw(std::span<double> data) const -> void;

    // Write data without applying any scale_factor and/or add_offset
    auto write_raw(std::span<int8_t const> data) -> void;
    auto write_raw(std::span<uint8_t const> data) -> void;
    auto write_raw(std::span<int16_t const> data) -> void;
    auto write_raw(std::span<uint16_t const> data) -> void;
    auto write_raw(std::span<int32_t const> data) -> void;
    auto write_raw(std::span<uint32_t const> data) -> void;
    auto write_raw(std::span<int64_t const> data) -> void;
    auto write_raw(std::span<uint64_t const> data) -> void;
    auto write_raw(std::span<float const> data) -> void;
    auto write_raw(std::span<double const> data) -> void;

  private:
    dataset(sweep const& parent, int varid);
    dataset(sweep const& parent, char const* name, data_type type, int compression);
    dataset(sweep const& parent, standard_quantity quantity, data_type type, int compression);

    auto check_size_mismatch(size_t size) const -> void;

    template <typename T>
    auto read_impl(std::span<T> data, T fill, T undetect) const -> void;
    template <typename T, class IsFillValue, class IsUndetect>
    auto write_impl(std::span<T const> data) -> void;

  private:
    int     ncid_;
    int     varid_;
    size_t  size_;

    friend class sweep;
  };

  /// Base class for groups that exist to group metadata variables
  class group
  {
  public:
    /// Directly access the NetCDF handle of this group
    auto ncid() -> int { return ncid_; }

    /// Check whether a metadata attribute exists
    auto attribute_exists(char const* name) const -> bool;
    auto attribute_exists(std::string const& name) const -> bool { return attribute_exists(name.c_str()); }

    /// Get a metadata attribute if present
    auto attribute_get(char const* name) const -> std::optional<metadata_value>;
    auto attribute_get(std::string const& name) const -> std::optional<metadata_value> { return attribute_get(name.c_str()); }

    /// Set a metadata attribute
    auto attribute_set(char const* name, metadata_value const& val) -> void;
    auto attribute_set(std::string const& name, metadata_value const& val) -> void { attribute_set(name.c_str(), val); }

    /// Erase a metadata attribute
    auto attribute_erase(char const* name) -> void;
    auto attribute_erase(std::string const& name) -> void { attribute_erase(name.c_str()); }

    /// Check whether a variable exists
    auto variable_exists(char const* name) const -> bool;
    auto variable_exists(std::string const& name) const -> bool { return variable_exists(name.c_str()); }

    /// Get a scalar metadata variable if present
    auto variable_get(char const* name) const -> std::optional<metadata_scalar>;
    auto variable_get(std::string const& name) const -> std::optional<metadata_scalar> { return variable_get(name.c_str()); }

    /// Set a scalar metadata variable
    auto variable_set(char const* name, metadata_scalar const& val) -> void;
    auto variable_set(std::string const& name, metadata_scalar const& val) -> void { variable_set(name.c_str(), val); }

  protected:
    explicit group(int ncid) : ncid_{ncid} { }

  protected:
    int ncid_;
  };

  class georeference_group : public group
  {
  public:
    auto latitude() -> std::optional<std::vector<double>>;
    auto set_latitude(std::vector<double> const& val) -> void;
    auto longitude() -> std::optional<std::vector<double>>;
    auto set_longitude(std::vector<double> const& val) -> void;
    auto altitude() -> std::optional<std::vector<double>>;
    auto set_altitude(std::vector<double> const& val) -> void;
    auto heading() -> std::optional<std::vector<float>>;
    auto set_heading(std::vector<float> const& val) -> void;
    auto roll() -> std::optional<std::vector<float>>;
    auto set_roll(std::vector<float> const& val) -> void;
    auto pitch() -> std::optional<std::vector<float>>;
    auto set_pitch(std::vector<float> const& val) -> void;
    auto drift() -> std::optional<std::vector<float>>;
    auto set_drift(std::vector<float> const& val) -> void;
    auto rotation() -> std::optional<std::vector<float>>;
    auto set_rotation(std::vector<float> const& val) -> void;
    auto tilt() -> std::optional<std::vector<float>>;
    auto set_tilt(std::vector<float> const& val) -> void;
    auto eastward_velocity() -> std::optional<std::vector<float>>;
    auto set_eastward_velocity(std::vector<float> const& val) -> void;
    auto northward_velocity() -> std::optional<std::vector<float>>;
    auto set_northward_velocity(std::vector<float> const& val) -> void;
    auto vertical_velocity() -> std::optional<std::vector<float>>;
    auto set_vertical_velocity(std::vector<float> const& val) -> void;
    auto eastward_wind() -> std::optional<std::vector<float>>;
    auto set_eastward_wind(std::vector<float> const& val) -> void;
    auto northward_wind() -> std::optional<std::vector<float>>;
    auto set_northward_wind(std::vector<float> const& val) -> void;
    auto vertical_wind() -> std::optional<std::vector<float>>;
    auto set_vertical_wind(std::vector<float> const& val) -> void;
    auto heading_rate() -> std::optional<std::vector<float>>;
    auto set_heading_rate(std::vector<float> const& val) -> void;
    auto roll_rate() -> std::optional<std::vector<float>>;
    auto set_roll_rate(std::vector<float> const& val) -> void;
    auto pitch_rate() -> std::optional<std::vector<float>>;
    auto set_pitch_rate(std::vector<float> const& val) -> void;
    auto georefs_applied() -> std::optional<std::vector<uint8_t>>;
    auto set_georefs_applied(std::vector<uint8_t> const& val) -> void;

  private:
    georeference_group(int ncid, int dimid_ray) : group{ncid}, dimid_ray_{dimid_ray} { }

  private:
    int dimid_ray_;
    friend class sweep;
  };

  class monitoring_group : public group
  {
  public:
    auto measured_transmit_power_h() -> std::optional<std::vector<float>>;
    auto set_measured_transmit_power_h(std::vector<float> const& val) -> void;
    auto measured_transmit_power_v() -> std::optional<std::vector<float>>;
    auto set_measured_transmit_power_v(std::vector<float> const& val) -> void;
    auto measured_sky_noise() -> std::optional<std::vector<float>>;
    auto set_measured_sky_noise(std::vector<float> const& val) -> void;
    auto measured_cold_noise() -> std::optional<std::vector<float>>;
    auto set_measured_cold_noise(std::vector<float> const& val) -> void;
    auto measured_hot_noise() -> std::optional<std::vector<float>>;
    auto set_measured_hot_noise(std::vector<float> const& val) -> void;
    auto phase_difference_transmit_hv() -> std::optional<std::vector<float>>;
    auto set_phase_difference_transmit_hv(std::vector<float> const& val) -> void;
    auto antenna_pointing_accuracy_elev() -> std::optional<std::vector<float>>;
    auto set_antenna_pointing_accuracy_elev(std::vector<float> const& val) -> void;
    auto antenna_pointing_accuracy_az() -> std::optional<std::vector<float>>;
    auto set_antenna_pointing_accuracy_az(std::vector<float> const& val) -> void;
    auto calibration_offset_h() -> std::optional<std::vector<float>>;
    auto set_calibration_offset_h(std::vector<float> const& val) -> void;
    auto calibration_offset_v() -> std::optional<std::vector<float>>;
    auto set_calibration_offset_v(std::vector<float> const& val) -> void;
    auto zdr_offset() -> std::optional<std::vector<float>>;
    auto set_zdr_offset(std::vector<float> const& val) -> void;

  private:
    monitoring_group(int ncid, int dimid_ray) : group{ncid}, dimid_ray_{dimid_ray} { }

  private:
    int dimid_ray_;
    friend class sweep;
  };

  class sweep : public group
  {
  public:
    auto ray_count() const { return rays_; }
    auto ray_variable_get(char const* name) const -> std::optional<metadata_vector>;
    auto ray_variable_get(std::string const& name) const -> std::optional<metadata_vector> { return ray_variable_get(name.c_str()); }
    auto ray_variable_set(char const* name, metadata_vector const& val) -> void;
    auto ray_variable_set(std::string const& name, metadata_vector const& val) -> void { ray_variable_set(name.c_str(), val); }

    auto ray_time() -> std::optional<std::pair<std::vector<std::chrono::duration<double>>, std::chrono::system_clock::time_point>>;
    auto ray_set_time(std::vector<std::chrono::duration<double>> const& val, std::chrono::system_clock::time_point since_time) -> void;
    auto ray_azimuth() -> std::optional<std::vector<float>>;
    auto ray_set_azimuth(std::vector<float> const& val) -> void;
    auto ray_elevation() -> std::optional<std::vector<float>>;
    auto ray_set_elevation(std::vector<float> const& val) -> void;
    auto ray_scan_rate() -> std::optional<std::vector<float>>;
    auto ray_set_scan_rate(std::vector<float> const& val) -> void;
    auto ray_antenna_transition() -> std::optional<std::vector<uint8_t>>;
    auto ray_set_antenna_transition(std::vector<uint8_t> const& val) -> void;
    auto ray_pulse_width() -> std::optional<std::vector<float>>;
    auto ray_set_pulse_width(std::vector<float> const& val) -> void;
    auto ray_calib_index() -> std::optional<std::vector<int>>;
    auto ray_set_calib_index(std::vector<int> const& val) -> void;
    auto ray_rx_range_resolution() -> std::optional<std::vector<float>>;
    auto ray_set_rx_range_resolution(std::vector<float> const& val) -> void;
    auto ray_prt() -> std::optional<std::vector<float>>;
    auto ray_set_prt(std::vector<float> const& val) -> void;
    auto ray_prt_ratio() -> std::optional<std::vector<float>>;
    auto ray_set_prt_ratio(std::vector<float> const& val) -> void;
    auto ray_prt_sequence() -> std::optional<std::vector<float>>;
    auto ray_set_prt_sequence(std::vector<float> const& val) -> void;
    auto ray_nyquist_velocity() -> std::optional<std::vector<float>>;
    auto ray_set_nyquist_velocity(std::vector<float> const& val) -> void;
    auto ray_unambiguous_range() -> std::optional<std::vector<float>>;
    auto ray_set_unambiguous_range(std::vector<float> const& val) -> void;
    auto ray_n_samples() -> std::optional<std::vector<int>>;
    auto ray_set_n_samples(std::vector<int> const& val) -> void;

    auto bin_count() const { return bins_; }
    auto bin_variable_get(char const* name) const -> std::optional<metadata_vector>;
    auto bin_variable_get(std::string const& name) const -> std::optional<metadata_vector> { return bin_variable_get(name.c_str()); }
    auto bin_variable_set(char const* name, metadata_vector const& val) -> void;
    auto bin_variable_set(std::string const& name, metadata_vector const& val) -> void { bin_variable_set(name.c_str(), val); }

    auto bin_range() -> std::optional<std::vector<float>>;
    auto bin_set_range(std::vector<float> const& val) -> void;

    auto frequency_count() const { return freqs_; }
    auto frequency_variable_get(char const* name) const -> std::optional<metadata_vector>;
    auto frequency_variable_get(std::string const& name) const -> std::optional<metadata_vector> { return frequency_variable_get(name.c_str()); }
    auto frequency_variable_set(char const* name, metadata_vector const& val) -> void;
    auto frequency_variable_set(std::string const& name, metadata_vector const& val) -> void { frequency_variable_set(name.c_str(), val); }

    auto frequency_frequency() -> std::optional<std::vector<float>>;
    auto frequency_set_frequency(std::vector<float> const& val) -> void;

    auto prt_count() const { return prts_; }
    auto prt_variable_get(char const* name) const -> std::optional<metadata_vector>;
    auto prt_variable_get(std::string const& name) const -> std::optional<metadata_vector> { return prt_variable_get(name.c_str()); }
    auto prt_variable_set(char const* name, metadata_vector const& val) -> void;
    auto prt_variable_set(std::string const& name, metadata_vector const& val) -> void { prt_variable_set(name.c_str(), val); }

    auto dataset_count() const { return datasets_.size(); }
    auto dataset_exists(char const* name) const -> bool;
    auto dataset_exists(std::string const& name) const -> bool { return dataset_exists(name.c_str()); }
    auto dataset(size_t i) -> std::optional<fm301::dataset> { return dataset(datasets_[i]); }
    auto dataset(size_t i) const -> std::optional<fm301::dataset const> { return dataset(datasets_[i]); }
    auto dataset(char const* name) -> std::optional<fm301::dataset>;
    auto dataset(char const* name) const -> std::optional<fm301::dataset const>;
    auto dataset(std::string const& name) -> std::optional<fm301::dataset> { return dataset(name.c_str()); }
    auto dataset(std::string const& name) const -> std::optional<fm301::dataset const> { return dataset(name.c_str()); }
    auto dataset(standard_quantity quantity) -> std::optional<fm301::dataset>;
    auto dataset_add(char const* name, data_type type, int compression) -> fm301::dataset;
    auto dataset_add(std::string const& name, data_type type, int compression) -> fm301::dataset { return dataset_add(name.c_str(), type, compression); }
    auto dataset_add(standard_quantity quantity, data_type type, int compression) -> fm301::dataset;

    auto georeference() const -> std::optional<georeference_group>;
    auto georeference_add() -> georeference_group;

    auto monitoring() const -> std::optional<monitoring_group>;
    auto monitoring_add() -> monitoring_group;

    auto sweep_number() const -> std::optional<int>;
    auto set_sweep_number(int val) -> void;

    auto sweep_mode() const -> std::optional<std::string>;
    auto set_sweep_mode(std::string const& val) -> void;

    auto follow_mode() const -> std::optional<std::string>;
    auto set_follow_mode(std::string const& val) -> void;

    auto prt_mode() const -> std::optional<std::string>;
    auto set_prt_mode(std::string const& val) -> void;

    auto fixed_angle() const -> std::optional<float>;
    auto set_fixed_angle(float val) -> void;

    auto polarization_mode() const -> std::optional<std::string>;
    auto set_polarization_mode(std::string const& val) -> void;

    auto polarization_sequence() const -> std::optional<std::vector<std::string>>;
    auto set_polarization_sequence(std::vector<std::string> const& val) -> void;

    auto rays_are_indexed() const -> std::optional<bool>;
    auto set_rays_are_indexed(bool val) -> void;

    auto ray_angle_resolution() const -> std::optional<float>;
    auto set_ray_angle_resolution(float val) -> void;

    auto qc_procedures() const -> std::optional<std::string>;
    auto set_qc_procedures(std::string const& val) -> void;

    auto target_scan_rate() const -> std::optional<float>;
    auto set_target_scan_rate(float val) -> void;

  private:
    explicit sweep(int ncid);
    sweep(int ncid, size_t rays, size_t bins, size_t freqs, size_t prts);

  private:
    int     dimid_ray_;
    int     dimid_bin_;
    int     dimid_freq_;
    int     dimid_prt_;
    size_t  rays_;
    size_t  bins_;
    size_t  freqs_;
    size_t  prts_;
    std::vector<std::string> datasets_; // list of known dataset variable ids and names

    friend class dataset;
    friend class volume;
  };

  /// Radar parameters metadata subgroup
  class radar_parameters_group : public group
  {
  public:
    auto antenna_gain_h() const -> std::optional<float>;
    auto set_antenna_gain_h(float val) -> void;

    auto antenna_gain_v() const -> std::optional<float>;
    auto set_antenna_gain_v(float val) -> void;

    auto beam_width_h() const -> std::optional<float>;
    auto set_beam_width_h(float val) -> void;

    auto beam_width_v() const -> std::optional<float>;
    auto set_beam_width_v(float val) -> void;

    auto receiver_bandwidth() const -> std::optional<float>;
    auto set_receiver_bandwidth(float val) -> void;

  protected:
    using group::group;
    friend class volume;
  };

  /// Lidar parameters metadata subgroup
  class lidar_parameters_group : public group
  {
  public:
    auto beam_divergence() const -> std::optional<float>;
    auto set_beam_divergence(float val) -> void;

    auto field_of_view() const -> std::optional<float>;
    auto set_field_of_view(float val) -> void;

    auto aperture_diameter() const -> std::optional<float>;
    auto set_aperture_diameter(float val) -> void;

    auto aperture_efficiency() const -> std::optional<float>;
    auto set_aperture_efficiency(float val) -> void;

    auto peak_power() const -> std::optional<float>;
    auto set_peak_power(float val) -> void;

    auto pulse_energy() const -> std::optional<float>;
    auto set_pulse_energy(float val) -> void;

  protected:
    using group::group;
    friend class volume;
  };

  /// Radar caliabration metadata subgroup
  class radar_calibration_group : public group
  {
  public:
    auto calibration_count() const -> size_t;

    auto calibration_variable_get(char const* name, size_t calib) const -> std::optional<metadata_scalar>;
    auto calibration_variable_get(std::string const& name, size_t calib) const -> std::optional<metadata_scalar> { return calibration_variable_get(name.c_str(), calib); }
    auto calibration_variable_set(char const* name, size_t calib, metadata_scalar const& val) -> void;
    auto calibration_variable_set(std::string const& name, size_t calib, metadata_scalar const& val) -> void { calibration_variable_set(name.c_str(), calib, val); }

    auto calib_index(size_t calib) const -> std::optional<uint8_t>;
    auto set_calib_index(size_t calib, uint8_t val) -> void;

    auto time(size_t calib) const -> std::optional<std::chrono::system_clock::time_point>;
    auto set_time(size_t calib, std::chrono::system_clock::time_point val) -> void;

    auto pulse_width(size_t calib) const -> std::optional<float>;
    auto set_pulse_width(size_t calib, float val) -> void;

    auto antenna_gain_h(size_t calib) const -> std::optional<float>;
    auto set_antenna_gain_h(size_t calib, float val) -> void;

    auto antenna_gain_v(size_t calib) const -> std::optional<float>;
    auto set_antenna_gain_v(size_t calib, float val) -> void;

    auto xmit_power_h(size_t calib) const -> std::optional<float>;
    auto set_xmit_power_h(size_t calib, float val) -> void;

    auto xmit_power_v(size_t calib) const -> std::optional<float>;
    auto set_xmit_power_v(size_t calib, float val) -> void;

    auto two_way_waveguide_loss_h(size_t calib) const -> std::optional<float>;
    auto set_two_way_waveguide_loss_h(size_t calib, float val) -> void;

    auto two_way_waveguide_loss_v(size_t calib) const -> std::optional<float>;
    auto set_two_way_waveguide_loss_v(size_t calib, float val) -> void;

    auto two_way_radome_loss_h(size_t calib) const -> std::optional<float>;
    auto set_two_way_radome_loss_h(size_t calib, float val) -> void;

    auto two_way_radome_loss_v(size_t calib) const -> std::optional<float>;
    auto set_two_way_radome_loss_v(size_t calib, float val) -> void;

    auto receiver_mismatch_loss(size_t calib) const -> std::optional<float>;
    auto set_receiver_mismatch_loss(size_t calib, float val) -> void;

    auto receiver_mismatch_loss_h(size_t calib) const -> std::optional<float>;
    auto set_receiver_mismatch_loss_h(size_t calib, float val) -> void;

    auto receiver_mismatch_loss_v(size_t calib) const -> std::optional<float>;
    auto set_receiver_mismatch_loss_v(size_t calib, float val) -> void;

    auto radar_constant_h(size_t calib) const -> std::optional<float>;
    auto set_radar_constant_h(size_t calib, float val) -> void;

    auto radar_constant_v(size_t calib) const -> std::optional<float>;
    auto set_radar_constant_v(size_t calib, float val) -> void;

    auto probert_jones_correction(size_t calib) const -> std::optional<float>;
    auto set_probert_jones_correction(size_t calib, float val) -> void;

    auto dielectric_factor_used(size_t calib) const -> std::optional<float>;
    auto set_dielectric_factor_used(size_t calib, float val) -> void;

    auto noise_hc(size_t calib) const -> std::optional<float>;
    auto set_noise_hc(size_t calib, float val) -> void;

    auto noise_vc(size_t calib) const -> std::optional<float>;
    auto set_noise_vc(size_t calib, float val) -> void;

    auto noise_hx(size_t calib) const -> std::optional<float>;
    auto set_noise_hx(size_t calib, float val) -> void;

    auto noise_vx(size_t calib) const -> std::optional<float>;
    auto set_noise_vx(size_t calib, float val) -> void;

    auto receiver_gain_hc(size_t calib) const -> std::optional<float>;
    auto set_receiver_gain_hc(size_t calib, float val) -> void;

    auto receiver_gain_vc(size_t calib) const -> std::optional<float>;
    auto set_receiver_gain_vc(size_t calib, float val) -> void;

    auto receiver_gain_hx(size_t calib) const -> std::optional<float>;
    auto set_receiver_gain_hx(size_t calib, float val) -> void;

    auto receiver_gain_vx(size_t calib) const -> std::optional<float>;
    auto set_receiver_gain_vx(size_t calib, float val) -> void;

    auto base_1km_hc(size_t calib) const -> std::optional<float>;
    auto set_base_1km_hc(size_t calib, float val) -> void;

    auto base_1km_vc(size_t calib) const -> std::optional<float>;
    auto set_base_1km_vc(size_t calib, float val) -> void;

    auto base_1km_hx(size_t calib) const -> std::optional<float>;
    auto set_base_1km_hx(size_t calib, float val) -> void;

    auto base_1km_vx(size_t calib) const -> std::optional<float>;
    auto set_base_1km_vx(size_t calib, float val) -> void;

    auto sun_power_hc(size_t calib) const -> std::optional<float>;
    auto set_sun_power_hc(size_t calib, float val) -> void;

    auto sun_power_vc(size_t calib) const -> std::optional<float>;
    auto set_sun_power_vc(size_t calib, float val) -> void;

    auto sun_power_hx(size_t calib) const -> std::optional<float>;
    auto set_sun_power_hx(size_t calib, float val) -> void;

    auto sun_power_vx(size_t calib) const -> std::optional<float>;
    auto set_sun_power_vx(size_t calib, float val) -> void;

    auto noise_source_power_h(size_t calib) const -> std::optional<float>;
    auto set_noise_source_power_h(size_t calib, float val) -> void;

    auto noise_source_power_v(size_t calib) const -> std::optional<float>;
    auto set_noise_source_power_v(size_t calib, float val) -> void;

    auto power_measure_loss_h(size_t calib) const -> std::optional<float>;
    auto set_power_measure_loss_h(size_t calib, float val) -> void;

    auto power_measure_loss_v(size_t calib) const -> std::optional<float>;
    auto set_power_measure_loss_v(size_t calib, float val) -> void;

    auto coupler_forward_loss_h(size_t calib) const -> std::optional<float>;
    auto set_coupler_forward_loss_h(size_t calib, float val) -> void;

    auto coupler_forward_loss_v(size_t calib) const -> std::optional<float>;
    auto set_coupler_forward_loss_v(size_t calib, float val) -> void;

    auto zdr_correction(size_t calib) const -> std::optional<float>;
    auto set_zdr_correction(size_t calib, float val) -> void;

    auto ldr_correction_h(size_t calib) const -> std::optional<float>;
    auto set_ldr_correction_h(size_t calib, float val) -> void;

    auto ldr_correction_v(size_t calib) const -> std::optional<float>;
    auto set_ldr_correction_v(size_t calib, float val) -> void;

    auto system_phidp(size_t calib) const -> std::optional<float>;
    auto set_system_phidp(size_t calib, float val) -> void;

    auto test_power_h(size_t calib) const -> std::optional<float>;
    auto set_test_power_h(size_t calib, float val) -> void;

    auto test_power_v(size_t calib) const -> std::optional<float>;
    auto set_test_power_v(size_t calib, float val) -> void;

    auto receiver_slope_hc(size_t calib) const -> std::optional<float>;
    auto set_receiver_slope_hc(size_t calib, float val) -> void;

    auto receiver_slope_vc(size_t calib) const -> std::optional<float>;
    auto set_receiver_slope_vc(size_t calib, float val) -> void;

    auto receiver_slope_hx(size_t calib) const -> std::optional<float>;
    auto set_receiver_slope_hx(size_t calib, float val) -> void;

    auto receiver_slope_vx(size_t calib) const -> std::optional<float>;
    auto set_receiver_slope_vx(size_t calib, float val) -> void;

  private:
    explicit radar_calibration_group(int ncid);
    radar_calibration_group(int ncid, size_t calibration_count);

  private:
    int dimid_calib_;

    friend class volume;
  };

  /// Lidar calibration metadata subgroup
  class lidar_calibration_group : public group
  {
  protected:
    using group::group;
    friend class volume;
  };

  /// Georeference correction metadata subgroup
  class georeference_correction_group : public group
  {
  public:
    auto azimuth_correction() const -> std::optional<float>;
    auto set_azimuth_correction(float val) -> void;

    auto elevation_correction() const -> std::optional<float>;
    auto set_elevation_correction(float val) -> void;

    auto range_correction() const -> std::optional<float>;
    auto set_range_correction(float val) -> void;

    auto longitude_correction() const -> std::optional<float>;
    auto set_longitude_correction(float val) -> void;

    auto latitude_correction() const -> std::optional<float>;
    auto set_latitude_correction(float val) -> void;

    auto pressure_altitude_correction() const -> std::optional<float>;
    auto set_pressure_altitude_correction(float val) -> void;

    auto radar_altitude_correction() const -> std::optional<float>;
    auto set_radar_altitude_correction(float val) -> void;

    auto eastward_ground_speed_correction() const -> std::optional<float>;
    auto set_eastward_ground_speed_correction(float val) -> void;

    auto northward_ground_speed_correction() const -> std::optional<float>;
    auto set_northward_ground_speed_correction(float val) -> void;

    auto vertical_velocity_correction() const -> std::optional<float>;
    auto set_vertical_velocity_correction(float val) -> void;

    auto heading_correction() const -> std::optional<float>;
    auto set_heading_correction(float val) -> void;

    auto roll_correction() const -> std::optional<float>;
    auto set_roll_correction(float val) -> void;

    auto pitch_correction() const -> std::optional<float>;
    auto set_pitch_correction(float val) -> void;

    auto drift_correction() const -> std::optional<float>;
    auto set_drift_correction(float val) -> void;

    auto rotation_correction() const -> std::optional<float>;
    auto set_rotation_correction(float val) -> void;

    auto tilt_correction() const -> std::optional<float>;
    auto set_tilt_correction(float val) -> void;

  protected:
    using group::group;
    friend class volume;
  };

  /// Top level volume object
  class volume : public group
  {
  public:
    /// Create or open an FM301 file
    volume(std::filesystem::path const& path, io_mode mode);

    /// Move construction
    volume(volume&& rhs) noexcept;

    /// Move assignment
    auto operator=(volume&& rhs) noexcept -> volume&;

    /// Destructor
    ~volume() noexcept;

    /// Flush the underlying NetCDF file to ensure updates have been fully written to disk
    auto flush() ->  void;

    // Sweep access ----------------------------------------------------------------------------------------------------

    auto sweep_count() const { return sweeps_; }
    auto sweep(size_t index) const -> fm301::sweep;
    auto sweep(std::string const& name) -> fm301::sweep;
    auto sweep_add(size_t rays, size_t bins, size_t frequencies, size_t prts, std::string name = std::string()) -> fm301::sweep;

    // Metadata sub-group access ---------------------------------------------------------------------------------------

    auto radar_parameters() const -> std::optional<radar_parameters_group>;
    auto radar_parameters_add() -> radar_parameters_group;

    auto radar_calibration() const -> std::optional<radar_calibration_group>;
    auto radar_calibration_add(size_t calibration_count) -> radar_calibration_group;

    auto lidar_parameters() const -> std::optional<lidar_parameters_group>;
    auto lidar_parameters_add() -> lidar_parameters_group;

    auto lidar_calibration() const -> std::optional<lidar_calibration_group>;
    auto lidar_calibration_add() -> lidar_calibration_group;

    // General metadata - CF Conventions -------------------------------------------------------------------------------

    auto conventions() const -> std::optional<std::string>;
    auto set_conventions(std::string const& val) -> void;

    auto version() const -> std::optional<std::string>;
    auto set_version(std::string const& val) -> void;

    auto title() const -> std::optional<std::string>;
    auto set_title(std::string const& val) -> void;

    auto institution() const -> std::optional<std::string>;
    auto set_institution(std::string const& val) -> void;

    auto references() const -> std::optional<std::string>;
    auto set_references(std::string const& val) -> void;

    auto source() const -> std::optional<std::string>;
    auto set_source(std::string const& val) -> void;

    auto history() const -> std::optional<std::string>;
    auto set_history(std::string const& val) -> void;

    auto comment() const -> std::optional<std::string>;
    auto set_comment(std::string const& val) -> void;

    auto instrument_name() const -> std::optional<std::string>;
    auto set_instrument_name(std::string const& val) -> void;

    auto site_name() const -> std::optional<std::string>;
    auto set_site_name(std::string const& val) -> void;

    auto scan_name() const -> std::optional<std::string>;
    auto set_scan_name(std::string const& val) -> void;

    auto scan_id() const -> std::optional<int>;
    auto set_scan_id(int val) -> void;

    auto platform_is_mobile() const -> std::optional<bool>;
    auto set_platform_is_mobile(bool val) -> void;

    auto ray_times_increase() const -> std::optional<bool>;
    auto set_ray_times_increase(bool val) -> void;

    auto simulated_data() const -> std::optional<bool>;
    auto set_simulated_data(bool val) -> void;

    // General metadata - WMO FM301 Standard ---------------------------------------------------------------------------

    auto wmo_cf_profile() const -> std::optional<std::string>;
    auto set_wmo_cf_profile(std::string const& val) -> void;

    auto wmo_data_category() const -> std::optional<int>;
    auto set_wmo_data_category(int val) -> void;

    auto wmo_originating_centre() const -> std::optional<int>;
    auto set_wmo_originating_centre(int val) -> void;

    auto wmo_originating_sub_centre() const -> std::optional<int>;
    auto set_wmo_originating_sub_centre(int val) -> void;

    auto wmo_update_sequence_number() const -> std::optional<int>;
    auto set_wmo_update_sequence_number(int val) -> void;

    auto wmo_id() const -> std::optional<std::string>;
    auto set_wmo_id(std::string const& val) -> void;

    auto wmo_wsi() const -> std::optional<std::string>;
    auto set_wmo_wsi(std::string const& val) -> void;

    //--------------------------------------------------------------------------

    auto volume_number() const -> std::optional<int>;
    auto set_volume_number(int val) -> void;

    auto platform_type() const -> std::optional<std::string>;
    auto set_platform_type(std::string const& val) -> void;

    auto instrument_type() const -> std::optional<std::string>;
    auto set_instrument_type(std::string const& val) -> void;

    auto primary_axis() const -> std::optional<std::string>;
    auto set_primary_axis(std::string const& val) -> void;

    auto time_coverage_start() const -> std::optional<std::chrono::system_clock::time_point>;
    auto set_time_coverage_start(std::chrono::system_clock::time_point val) -> void;

    auto time_coverage_end() const -> std::optional<std::chrono::system_clock::time_point>;
    auto set_time_coverage_end(std::chrono::system_clock::time_point val) -> void;

    auto latitude() const -> std::optional<double>;
    auto set_latitude(double val) -> void;

    auto longitude() const -> std::optional<double>;
    auto set_longitude(double val) -> void;

    auto altitude() const -> std::optional<double>;
    auto set_altitude(double val) -> void;

    auto altitude_agl() const -> std::optional<double>;
    auto set_altitude_agl(double val) -> void;

    auto status_str() const -> std::optional<std::string>;
    auto set_status_str(std::string const& val) -> void;

  private:
    int     dimid_sweep_;
    size_t  sweeps_;
  };

  template <typename T>
  auto dataset::read_impl(std::span<T> data, T fill, T undetect) const -> void
  {
    auto scale = scale_factor().value_or(1);
    auto offset = add_offset().value_or(0);

    auto impl = [&](auto dummy)
    {
      using ptype = decltype(dummy);
      auto var_fill = fill_value();
      auto var_undetect = this->undetect();
      auto buf = std::make_unique<ptype[]>(data.size());

      read_raw(std::span{buf.get(), data.size()});

      if (var_fill && var_undetect)
      {
        auto fillval = std::get<ptype>(*var_fill);
        auto undetectval = std::get<ptype>(*var_undetect);
        for (size_t i = 0; i < data.size(); ++i)
          data[i]
            = buf[i] == fillval ? fill
            : buf[i] == undetectval ? undetect
            : buf[i] * scale + offset;
      }
      else if (var_fill)
      {
        auto fillval = std::get<ptype>(*var_fill);
        for (size_t i = 0; i < data.size(); ++i)
          data[i] = buf[i] == fillval ? fill : buf[i] * scale + offset;
      }
      else if (var_undetect)
      {
        auto undetectval = std::get<ptype>(*var_undetect);
        for (size_t i = 0; i < data.size(); ++i)
          data[i] = buf[i] == undetectval ? undetect : buf[i] * scale + offset;
      }
      else
      {
        for (size_t i = 0; i < data.size(); ++i)
          data[i] = buf[i] * scale + offset;
      }
    };

    switch (type())
    {
    case data_type::i8:  impl(int8_t(0)); break;
    case data_type::u8:  impl(uint8_t(0)); break;
    case data_type::i16: impl(int16_t(0)); break;
    case data_type::u16: impl(uint16_t(0)); break;
    case data_type::i32: impl(int32_t(0)); break;
    case data_type::u32: impl(uint32_t(0)); break;
    case data_type::i64: impl(int64_t(0)); break;
    case data_type::u64: impl(uint64_t(0)); break;
    case data_type::f32: impl(float(0)); break;
    case data_type::f64: impl(double(0)); break;
    }
  }

  template <typename T, class IsFillValue, class IsUndetect>
  auto dataset::write_impl(std::span<T const> data) -> void
  {
    auto is_fill = IsFillValue{};
    auto is_undetect = IsUndetect{};
    auto scale = scale_factor().value_or(1);
    auto offset = add_offset().value_or(0);

    auto impl = [&](auto dummy)
    {
      using ptype = decltype(dummy);
      auto var_fill = fill_value();
      auto var_undetect = undetect();
      auto buf = std::make_unique<ptype[]>(data.size());

      // if packing into integral types we must round to nearest to ensure maximum accuracy on unpack
      if (var_fill && var_undetect)
      {
        auto fillval = std::get<ptype>(*var_fill);
        auto undetectval = std::get<ptype>(*var_undetect);
        for (size_t i = 0; i < data.size(); ++i)
          if constexpr (std::is_integral<ptype>::value)
            buf[i]
              = is_fill(data[i]) ? fillval
              : is_undetect(data[i]) ? undetectval
              : std::lround((data[i] - offset) / scale);
          else
            buf[i]
              = is_fill(data[i]) ? fillval
              : is_undetect(data[i]) ? undetectval
              : (data[i] - offset) / scale;
      }
      else if (var_fill)
      {
        auto fillval = std::get<ptype>(*var_fill);
        for (size_t i = 0; i < data.size(); ++i)
          if constexpr (std::is_integral<ptype>::value)
            buf[i] = is_fill(data[i]) ? fillval : std::lround((data[i] - offset) / scale);
          else
            buf[i] = is_fill(data[i]) ? fillval : (data[i] - offset) / scale;
      }
      else if (var_undetect)
      {
        auto undetectval = std::get<ptype>(*var_undetect);
        for (size_t i = 0; i < data.size(); ++i)
          if constexpr (std::is_integral<ptype>::value)
            buf[i] = is_undetect(data[i]) ? undetectval : std::lround((data[i] - offset) / scale);
          else
            buf[i] = is_undetect(data[i]) ? undetectval : (data[i] - offset) / scale;
      }
      else
      {
        for (size_t i = 0; i < data.size(); ++i)
          if constexpr (std::is_integral<ptype>::value)
            buf[i] = std::lround((data[i] - offset) / scale);
          else
            buf[i] = (data[i] - offset) / scale;
      }

      write_raw(std::span{buf.get(), data.size()});
    };

    switch (type())
    {
    case data_type::i8:  impl(int8_t(0)); break;
    case data_type::u8:  impl(uint8_t(0)); break;
    case data_type::i16: impl(int16_t(0)); break;
    case data_type::u16: impl(uint16_t(0)); break;
    case data_type::i32: impl(int32_t(0)); break;
    case data_type::u32: impl(uint32_t(0)); break;
    case data_type::i64: impl(int64_t(0)); break;
    case data_type::u64: impl(uint64_t(0)); break;
    case data_type::f32: impl(float(0)); break;
    case data_type::f64: impl(double(0)); break;
    }
  }
}
