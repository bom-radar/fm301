/*---------------------------------------------------------------------------------------------------------------------
 * FM301 C++ API and Toolkit
 *
 * Copyright 2021 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *-------------------------------------------------------------------------------------------------------------------*/
#include "ncutils.h"
#include <cstring>
#include <sstream>

#include <iostream>

using namespace fm301;

// we rely on NC_NOERR being 0 to allow us to use the convenient "if (auto status = nc_xxx())" syntax
static_assert(NC_NOERR == 0, "expected NC_NOERR to be defined as 0!");

error::error()
  : description_{"unknown error"}
{ }

error::error(char const* desc, char const* status, int ncid, int varid, char const* name)
{
  std::ostringstream oss;
  oss << desc;
  if (name)
    oss << " '" << name << "'";
  if (ncid != -1)
    oss << " at " << lookup_location(ncid, varid);
  if (status)
    oss << ": " << status;
  description_ = oss.str();
}

error::error(char const* desc, int status, int ncid, int varid, char const* name)
  : error{desc, status == NC_NOERR ? nullptr : nc_strerror(status), ncid, varid, name}
{ }

auto error::what() const noexcept -> char const*
{
  return description_.c_str();
}

auto fm301::data_type_to_nc_type(data_type in) -> nc_type
{
  switch (in)
  {
  case data_type::i8:
    return NC_BYTE;
  case data_type::u8:
    return NC_UBYTE;
  case data_type::i16:
    return NC_SHORT;
  case data_type::u16:
    return NC_USHORT;
  case data_type::i32:
    return NC_INT;
  case data_type::u32:
    return NC_UINT;
  case data_type::i64:
    return NC_INT64;
  case data_type::u64:
    return NC_UINT64;
  case data_type::f32:
    return NC_FLOAT;
  case data_type::f64:
    return NC_DOUBLE;
  }
  throw std::logic_error{"Unreachable"};
}

auto fm301::lookup_location(int ncid, int varid) -> std::string
{
  size_t len;
  if (nc_inq_grpname_full(ncid, &len, nullptr))
    throw std::runtime_error{"Failed to lookup netcdf location"};

  std::string loc(len, '\0');
  if (nc_inq_grpname_full(ncid, nullptr, &loc[0]))
    throw std::runtime_error{"Failed to lookup netcdf location"};

  if (varid != NC_GLOBAL)
  {
    char buf[NC_MAX_NAME];
    if (nc_inq_varname(ncid, varid, buf))
      throw std::runtime_error{"Failed to lookup netcdf location"};
    loc.append(1, '/').append(buf);
  }

  return loc;
}

auto fm301::lookup_type_name(int ncid, int type) -> std::string
{
  char name[NC_MAX_NAME];
  if (auto status = nc_inq_type(ncid, type, name, nullptr))
    throw error{"Failed to retrieve type name", status, ncid};
  return name;
}

auto fm301::group_exists(int ncid, char const* name) -> bool
{
  int grpid;
  if (auto status = nc_inq_grp_ncid(ncid, name, &grpid))
    return false;
  return true;
}

auto fm301::dim_exists(int ncid, char const* name) -> bool
{
  int dimid;
  if (auto status = nc_inq_dimid(ncid, name, &dimid))
  {
    if (status == NC_EBADDIM)
      return false;
    throw error{"Failed to lookup dimension", status, ncid, NC_GLOBAL, name};
  }
  return true;
}

auto fm301::att_exists(int ncid, int varid, char const* name) -> bool
{
  if (auto status = nc_inq_att(ncid, varid, name, nullptr, nullptr))
  {
    if (status == NC_ENOTATT)
      return false;
    throw error{"Failed to read attribute", status, ncid, varid, name};
  }
  return true;
}

auto fm301::att_erase(int ncid, int varid, char const* name) -> void
{
  if (auto status = nc_del_att(ncid, varid, name))
  {
    if (status != NC_ENOTATT)
      throw error{"Failed to erase attribute", status, ncid, varid, name};
  }
}

auto fm301::att_set(int ncid, int varid, char const* name, signed char const* data, size_t len) -> void
{
  if (auto status = nc_put_att_schar(ncid, varid, name, native_to_nc_type<decltype(*data)>(), len, data))
    throw error{"Failed to write attribute", status, ncid, varid, name};
}

auto fm301::att_set(int ncid, int varid, char const* name, unsigned char const* data, size_t len) -> void
{
  if (auto status = nc_put_att_uchar(ncid, varid, name, native_to_nc_type<decltype(*data)>(), len, data))
    throw error{"Failed to write attribute", status, ncid, varid, name};
}

auto fm301::att_set(int ncid, int varid, char const* name, short const* data, size_t len) -> void
{
  if (auto status = nc_put_att_short(ncid, varid, name, native_to_nc_type<decltype(*data)>(), len, data))
    throw error{"Failed to write attribute", status, ncid, varid, name};
}

auto fm301::att_set(int ncid, int varid, char const* name, unsigned short const* data, size_t len) -> void
{
  if (auto status = nc_put_att_ushort(ncid, varid, name, native_to_nc_type<decltype(*data)>(), len, data))
    throw error{"Failed to write attribute", status, ncid, varid, name};
}

auto fm301::att_set(int ncid, int varid, char const* name, int const* data, size_t len) -> void
{
  if (auto status = nc_put_att_int(ncid, varid, name, native_to_nc_type<decltype(*data)>(), len, data))
    throw error{"Failed to write attribute", status, ncid, varid, name};
}

auto fm301::att_set(int ncid, int varid, char const* name, unsigned int const* data, size_t len) -> void
{
  if (auto status = nc_put_att_uint(ncid, varid, name, native_to_nc_type<decltype(*data)>(), len, data))
    throw error{"Failed to write attribute", status, ncid, varid, name};
}

auto fm301::att_set(int ncid, int varid, char const* name, long const* data, size_t len) -> void
{
  if (auto status = nc_put_att_long(ncid, varid, name, native_to_nc_type<decltype(*data)>(), len, data))
    throw error{"Failed to write attribute", status, ncid, varid, name};
}

auto fm301::att_set(int ncid, int varid, char const* name, unsigned long const* data, size_t len) -> void
{
  // netcdf api is missing support for unsigned long - so we have to hack around it like this
  if constexpr (sizeof(unsigned long) == sizeof(unsigned int))
    att_set(ncid, varid, name, reinterpret_cast<unsigned int const*>(data), len);
  else if constexpr (sizeof(unsigned long) == sizeof(unsigned long long))
    att_set(ncid, varid, name, reinterpret_cast<unsigned long long const*>(data), len);
  else
    throw std::logic_error{"No conversion for unsigned long available"};
}

auto fm301::att_set(int ncid, int varid, char const* name, long long const* data, size_t len) -> void
{
  if (auto status = nc_put_att_longlong(ncid, varid, name, native_to_nc_type<decltype(*data)>(), len, data))
    throw error{"Failed to write attribute", status, ncid, varid, name};
}

auto fm301::att_set(int ncid, int varid, char const* name, unsigned long long const* data, size_t len) -> void
{
  if (auto status = nc_put_att_ulonglong(ncid, varid, name, native_to_nc_type<decltype(*data)>(), len, data))
    throw error{"Failed to write attribute", status, ncid, varid, name};
}

auto fm301::att_set(int ncid, int varid, char const* name, float const* data, size_t len) -> void
{
  if (auto status = nc_put_att_float(ncid, varid, name, native_to_nc_type<decltype(*data)>(), len, data))
    throw error{"Failed to write attribute", status, ncid, varid, name};
}

auto fm301::att_set(int ncid, int varid, char const* name, double const* data, size_t len) -> void
{
  if (auto status = nc_put_att_double(ncid, varid, name, native_to_nc_type<decltype(*data)>(), len, data))
    throw error{"Failed to write attribute", status, ncid, varid, name};
}

auto fm301::att_set(int ncid, int varid, char const* name, char const** data, size_t len) -> void
{
  if (auto status = nc_put_att_string(ncid, varid, name, len, data))
    throw error{"Failed to write attribute", status, ncid, varid, name};
}

auto fm301::att_set(int ncid, int varid, char const* name, std::string const* data, size_t len) -> void
{
  auto ptrs = std::make_unique<char const*[]>(len);
  for (size_t i = 0; i < len; ++i)
    ptrs[i] = data[i].c_str();
  if (auto status = nc_put_att_string(ncid, varid, name, len, ptrs.get()))
    throw error{"Failed to write attribute", status, ncid, varid, name};
}

auto fm301::att_set(int ncid, int varid, char const* name, char const* data) -> void
{
  if (auto status = nc_put_att_string(ncid, varid, name, 1, &data))
    throw error{"Failed to write attribute", status, ncid, varid, name};
}

auto fm301::att_set(int ncid, int varid, char const* name, std::string const& data) -> void
{
  auto valstr = data.c_str();
  if (auto status = nc_put_att_string(ncid, varid, name, 1, &valstr))
    throw error{"Failed to write attribute", status, ncid, varid, name};
}

auto fm301::var_exists(int ncid, char const* name) -> bool
{
  int varid;
  if (auto status = nc_inq_varid(ncid, name, &varid))
  {
    if (status == NC_ENOTVAR)
      return false;
    else
      throw error{"Failed to find variable", status, ncid, NC_GLOBAL, name};
  }
  return true;
}
