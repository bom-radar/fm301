/*---------------------------------------------------------------------------------------------------------------------
 * FM301 C++ API and Toolkit
 *
 * Copyright 2021 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *-------------------------------------------------------------------------------------------------------------------*/
#pragma once

#include "fm301.h"
#include <netcdf.h>
#include <cstring>
#include <exception>
#include <optional>
#include <string>

namespace fm301
{
  // the netcdf api lacks functions supporting the native 'unsigned long' type for some inexplicable reason
  // this forces us to work around it by aliasing it to eiither uint or ulonglong
  using ulong_alias_t = std::conditional<sizeof(unsigned long) == sizeof(unsigned int), unsigned int, unsigned long long>::type;
  static_assert(sizeof(ulong_alias_t) == sizeof(unsigned long), "No integer type can act as alias for unsigned long!");

  // trait used to determine whether a type is a specialization of a particular template
  template<typename, template<typename...> class>
  struct is_specialization : std::false_type {};
  template<template<typename...> class T, typename... Args>
  struct is_specialization<T<Args...>, T>: std::true_type {};

  /// Base exception thrown by FM301 API
  class error : public std::exception
  {
  public:
    error();
    error(char const* desc, char const* status = nullptr, int ncid = -1, int varid = NC_GLOBAL, char const* name = nullptr);
    error(char const* desc, int status, int ncid = -1, int varid = NC_GLOBAL, char const* name = nullptr);
    auto what() const noexcept -> char const* override;

  private:
    std::string description_;
  };

  // convert a C++ native numeric type into the NetCDF type with the same size and signedness
  template <typename T>
  constexpr auto native_to_nc_type() -> nc_type
  {
    using D = std::decay_t<T>;
    if (std::is_integral_v<D>)
    {
      switch (sizeof(D))
      {
      case 1: return std::is_unsigned_v<D> ? NC_UBYTE : NC_BYTE;
      case 2: return std::is_unsigned_v<D> ? NC_USHORT : NC_SHORT;
      case 4: return std::is_unsigned_v<D> ? NC_UINT : NC_INT;
      case 8: return std::is_unsigned_v<D> ? NC_UINT64 : NC_INT64;
      default: throw std::logic_error{"Unexpected type"};
      }
    }
    else if (std::is_floating_point_v<D>)
    {
      switch (sizeof(D))
      {
      case 4: return NC_FLOAT;
      case 8: return NC_DOUBLE;
      default: throw std::logic_error{"Unexpected type"};
      }
    }
    else if (std::is_same_v<D, std::string>)
      return NC_STRING;
    else
      throw std::logic_error{"Unexpected type"};
  }

  // convert a data_type enum into a native C++ type
  template <data_type type> struct data_type_to_native { };
  template <> struct data_type_to_native<data_type::i8>  { using type = int8_t; };
  template <> struct data_type_to_native<data_type::u8>  { using type = uint8_t; };
  template <> struct data_type_to_native<data_type::i16> { using type = int16_t; };
  template <> struct data_type_to_native<data_type::u16> { using type = uint16_t; };
  template <> struct data_type_to_native<data_type::i32> { using type = int32_t; };
  template <> struct data_type_to_native<data_type::u32> { using type = uint32_t; };
  template <> struct data_type_to_native<data_type::i64> { using type = int64_t; };
  template <> struct data_type_to_native<data_type::u64> { using type = uint64_t; };
  template <> struct data_type_to_native<data_type::f32> { using type = float; };
  template <> struct data_type_to_native<data_type::f64> { using type = double; };

  template <data_type type>
  using data_type_to_native_t = typename data_type_to_native<type>::type;

  auto data_type_to_nc_type(data_type in) -> nc_type;

  // get the path to a group
  auto lookup_location(int ncid, int varid = NC_GLOBAL) -> std::string;

  // determine a type name
  auto lookup_type_name(int ncid, int type) -> std::string;

  auto group_exists(int ncid, char const* name) -> bool;

  auto dim_exists(int ncid, char const* name) -> bool;

  inline auto wrap_nc_def_grp(int ncid, char const* name)
  {
    int grpid;
    if (auto status = nc_def_grp(ncid, name, &grpid))
      throw error{"Failed to create group", status, ncid, NC_GLOBAL, name};
    return grpid;
  }
  inline auto wrap_nc_def_dim(int ncid, char const* name, size_t len)
  {
    int dimid;
    if (auto status = nc_def_dim(ncid, name, len, &dimid))
      throw error{"Failed to create dimension", status, ncid, NC_GLOBAL, name};
    return dimid;
  }
  inline auto wrap_nc_inq_dimid(int ncid, char const* name)
  {
    int dimid;
    if (auto status = nc_inq_dimid(ncid, name, &dimid))
      throw error{"Failed to lookup dimension", status, ncid, NC_GLOBAL, name};
    return dimid;
  }
  inline auto wrap_nc_inq_dimlen(int ncid, int dimid)
  {
    size_t len;
    if (auto status = nc_inq_dimlen(ncid, dimid, &len))
      throw error{"Failed to determine length of dimension", status, ncid};
    return len;
  }
  inline auto wrap_nc_inq_varid(int ncid, char const* name)
  {
    int varid;
    if (auto status = nc_inq_varid(ncid, name, &varid))
      throw error{"Failed to lookup variable", status, ncid, NC_GLOBAL, name};
    return varid;
  }

  auto att_exists(int ncid, int varid, char const* name) -> bool;
  auto att_erase(int ncid, int varid, char const* name) -> void;

  // convert annoying C functions named by type into a single overloaded name
  inline int wrap_nc_get_att(int ncid, int varid, char const* name, signed char* value) { return nc_get_att_schar(ncid, varid, name, value); }
  inline int wrap_nc_get_att(int ncid, int varid, char const* name, unsigned char* value) { return nc_get_att_uchar(ncid, varid, name, value); }
  inline int wrap_nc_get_att(int ncid, int varid, char const* name, short* value) { return nc_get_att_short(ncid, varid, name, value); }
  inline int wrap_nc_get_att(int ncid, int varid, char const* name, unsigned short* value) { return nc_get_att_ushort(ncid, varid, name, value); }
  inline int wrap_nc_get_att(int ncid, int varid, char const* name, int* value) { return nc_get_att_int(ncid, varid, name, value); }
  inline int wrap_nc_get_att(int ncid, int varid, char const* name, unsigned int* value) { return nc_get_att_uint(ncid, varid, name, value); }
  inline int wrap_nc_get_att(int ncid, int varid, char const* name, long* value){ return nc_get_att_long(ncid, varid, name, value); }
  inline int wrap_nc_get_att(int ncid, int varid, char const* name, long long* value) { return nc_get_att_longlong(ncid, varid, name, value); }
  inline int wrap_nc_get_att(int ncid, int varid, char const* name, unsigned long long* value) { return nc_get_att_ulonglong(ncid, varid, name, value); }
  inline int wrap_nc_get_att(int ncid, int varid, char const* name, float* value) { return nc_get_att_float(ncid, varid, name, value); }
  inline int wrap_nc_get_att(int ncid, int varid, char const* name, double* value) { return nc_get_att_double(ncid, varid, name, value); }
  inline int wrap_nc_get_att(int ncid, int varid, char const* name, unsigned long* value)
  {
    return wrap_nc_get_att(ncid, varid, name, reinterpret_cast<ulong_alias_t*>(value));
  }

  #define WRAP_NC_GET_VAR(Type, Name) \
  inline void wrap_nc_get_var(int ncid, int varid, Type* op) \
  { \
    if (auto status = nc_get_var_##Name(ncid, varid, op)) \
      throw error{"Failed to read variable", status, ncid, varid}; \
  } \
  inline void wrap_nc_get_var1(int ncid, int varid, size_t const* indexp, Type* op) \
  { \
    if (auto status = nc_get_var1_##Name(ncid, varid, indexp, op)) \
      throw error{"Failed to read variable", status, ncid, varid}; \
  }
  WRAP_NC_GET_VAR(signed char, schar)
  WRAP_NC_GET_VAR(unsigned char, uchar)
  WRAP_NC_GET_VAR(short, short)
  WRAP_NC_GET_VAR(unsigned short, ushort)
  WRAP_NC_GET_VAR(int, int)
  WRAP_NC_GET_VAR(unsigned int, uint)
  WRAP_NC_GET_VAR(long, long)
  WRAP_NC_GET_VAR(long long, longlong)
  WRAP_NC_GET_VAR(unsigned long long, ulonglong)
  WRAP_NC_GET_VAR(float, float)
  WRAP_NC_GET_VAR(double, double)
  WRAP_NC_GET_VAR(char*, string)
  inline void wrap_nc_get_var(int ncid, int varid, unsigned long* op)
  {
    wrap_nc_get_var(ncid, varid, reinterpret_cast<ulong_alias_t*>(op));
  }
  inline void wrap_nc_get_var1(int ncid, int varid, size_t const* indexp, unsigned long* op)
  {
    wrap_nc_get_var1(ncid, varid, indexp, reinterpret_cast<ulong_alias_t*>(op));
  }

  #define WRAP_NC_PUT_VAR(Type, Name) \
  inline void wrap_nc_put_var(int ncid, int varid, Type const* op) \
  { \
    if (auto status = nc_put_var_##Name(ncid, varid, op)) \
      throw error{"Failed to write variable", status, ncid, varid}; \
  } \
  inline void wrap_nc_put_var1(int ncid, int varid, size_t const* indexp, Type const* op) \
  { \
    if (auto status = nc_put_var1_##Name(ncid, varid, indexp, op)) \
      throw error{"Failed to write variable", status, ncid, varid}; \
  }
  WRAP_NC_PUT_VAR(signed char, schar)
  WRAP_NC_PUT_VAR(unsigned char, uchar)
  WRAP_NC_PUT_VAR(short, short)
  WRAP_NC_PUT_VAR(unsigned short, ushort)
  WRAP_NC_PUT_VAR(int, int)
  WRAP_NC_PUT_VAR(unsigned int, uint)
  WRAP_NC_PUT_VAR(long, long)
  WRAP_NC_PUT_VAR(long long, longlong)
  WRAP_NC_PUT_VAR(unsigned long long, ulonglong)
  WRAP_NC_PUT_VAR(float, float)
  WRAP_NC_PUT_VAR(double, double)
  inline void wrap_nc_put_var(int ncid, int varid, unsigned long const* op)
  {
    wrap_nc_put_var(ncid, varid, reinterpret_cast<ulong_alias_t const*>(op));
  }
  inline void wrap_nc_put_var1(int ncid, int varid, size_t const* indexp, unsigned long const* op)
  {
    wrap_nc_put_var1(ncid, varid, indexp, reinterpret_cast<ulong_alias_t const*>(op));
  }
  inline void wrap_nc_put_var(int ncid, int varid, char const** op)
  {
    if (auto status = nc_put_var_string(ncid, varid, op))
      throw error{"Failed to write variable", status, ncid, varid};
  }
  inline void wrap_nc_put_var1(int ncid, int varid, size_t const* indexp, char const** op)
  {
    if (auto status = nc_put_var1_string(ncid, varid, indexp, op))
      throw error{"Failed to write variable", status, ncid, varid};
  }

  template <typename T>
  auto att_get_impl(int ncid, int varid, char const* name, nc_type type, size_t len) -> std::optional<T>
  {
    std::optional<T> val{std::in_place};
    if constexpr (std::is_same_v<T, std::string>)
    {
      if (type == NC_CHAR)
      {
        val->resize(len);
        if (auto status = nc_get_att_text(ncid, varid, name, &val->data()[0]))
          throw error{"Failed to read attribute", status, ncid, varid, name};
      }
      else if (type == NC_STRING)
      {
        if (len > 1)
          throw error{"Expected scalar attribute", NC_NOERR, ncid, varid, name};
        char* ptr;
        if (auto status = nc_get_att_string(ncid, varid, name, &ptr))
          throw error{"Failed to read attribute", status, ncid, varid, name};
        val->assign(ptr);
        nc_free_string(1, &ptr);
      }
      else
        throw error{"Invalid string attribute", NC_NOERR, ncid, varid, name};
    }
    else if constexpr (std::is_same_v<T, std::vector<std::string>>)
    {
      val->resize(len);
      auto ptrs = std::make_unique<char*[]>(len);
      if (auto status = nc_get_att_string(ncid, varid, name, ptrs.get()))
        throw error{"Failed to read attribute", status, ncid, varid, name};
      for (size_t i = 0; i < len; ++i)
        (*val)[i].assign(ptrs[i]);
      nc_free_string(len, ptrs.get());
    }
    else if constexpr (is_specialization<T, std::vector>::value)
    {
      val->resize(len);
      if (auto status = wrap_nc_get_att(ncid, varid, name, val->data()))
        throw error{"Failed to read attribute", status, ncid, varid, name};
    }
    else
    {
      if (len > 1)
        throw error{"Expected scalar attribute", NC_NOERR, ncid, varid, name};
      if (auto status = wrap_nc_get_att(ncid, varid, name, &(*val)))
        throw error{"Failed to read attribute", status, ncid, varid, name};
    }
    return val;
  }

  template <typename T>
  auto att_get(int ncid, int varid, char const* name) -> std::optional<T>
  {
    nc_type type; size_t len;
    if (auto status = nc_inq_att(ncid, varid, name, &type, &len))
    {
      if (status == NC_ENOTATT)
        return std::nullopt;
      throw error{"Failed to read attribute", status, ncid, varid, name};
    }
    return att_get_impl<T>(ncid, varid, name, type, len);
  }
  template <>
  inline auto att_get(int ncid, int varid, char const* name) -> std::optional<data_type_scalar>
  {
    nc_type type; size_t len;
    if (auto status = nc_inq_att(ncid, varid, name, &type, &len))
    {
      if (status == NC_ENOTATT)
        return std::nullopt;
      throw error{"Failed to read attribute", status, ncid, varid, name};
    }

    switch (type)
    {
    case NC_BYTE:   return att_get_impl<int8_t>(ncid, varid, name, type, len);
    case NC_UBYTE:  return att_get_impl<uint8_t>(ncid, varid, name, type, len);
    case NC_SHORT:  return att_get_impl<int16_t>(ncid, varid, name, type, len);
    case NC_USHORT: return att_get_impl<uint16_t>(ncid, varid, name, type, len);
    case NC_INT:    return att_get_impl<int32_t>(ncid, varid, name, type, len);
    case NC_UINT:   return att_get_impl<uint32_t>(ncid, varid, name, type, len);
    case NC_INT64:  return att_get_impl<int64_t>(ncid, varid, name, type, len);
    case NC_UINT64: return att_get_impl<uint64_t>(ncid, varid, name, type, len);
    case NC_FLOAT:  return att_get_impl<float>(ncid, varid, name, type, len);
    case NC_DOUBLE: return att_get_impl<double>(ncid, varid, name, type, len);
    default:        throw error{"Unexpected type for attribute", NC_NOERR, ncid, varid, name};
    }
  }
  template <>
  inline auto att_get(int ncid, int varid, char const* name) -> std::optional<data_type_vector>
  {
    nc_type type; size_t len;
    if (auto status = nc_inq_att(ncid, varid, name, &type, &len))
    {
      if (status == NC_ENOTATT)
        return std::nullopt;
      throw error{"Failed to read attribute", status, ncid, varid, name};
    }

    switch (type)
    {
    case NC_BYTE:   return att_get_impl<std::vector<int8_t>>(ncid, varid, name, type, len);
    case NC_UBYTE:  return att_get_impl<std::vector<uint8_t>>(ncid, varid, name, type, len);
    case NC_SHORT:  return att_get_impl<std::vector<int16_t>>(ncid, varid, name, type, len);
    case NC_USHORT: return att_get_impl<std::vector<uint16_t>>(ncid, varid, name, type, len);
    case NC_INT:    return att_get_impl<std::vector<int32_t>>(ncid, varid, name, type, len);
    case NC_UINT:   return att_get_impl<std::vector<uint32_t>>(ncid, varid, name, type, len);
    case NC_INT64:  return att_get_impl<std::vector<int64_t>>(ncid, varid, name, type, len);
    case NC_UINT64: return att_get_impl<std::vector<uint64_t>>(ncid, varid, name, type, len);
    case NC_FLOAT:  return att_get_impl<std::vector<float>>(ncid, varid, name, type, len);
    case NC_DOUBLE: return att_get_impl<std::vector<double>>(ncid, varid, name, type, len);
    default:        throw error{"Unexpected type for attribute", NC_NOERR, ncid, varid, name};
    }
  }
  template <>
  inline auto att_get(int ncid, int varid, char const* name) -> std::optional<metadata_value>
  {
    nc_type type; size_t len;
    if (auto status = nc_inq_att(ncid, varid, name, &type, &len))
    {
      if (status == NC_ENOTATT)
        return std::nullopt;
      throw error{"Failed to read attribute", status, ncid, varid, name};
    }

    /* NetCDF make no distinction between a scalar attributes, and arrays of size 1.  This means we are forced to
     * make an assumption and return a scalar if the length is 1.  It is a potential headache for users since they
     * might expect a vector to be read, but get a scalar returned. */
    if (len > 1)
    {
      switch (type)
      {
      case NC_BYTE:   return att_get_impl<std::vector<int8_t>>(ncid, varid, name, type, len);
      case NC_UBYTE:  return att_get_impl<std::vector<uint8_t>>(ncid, varid, name, type, len);
      case NC_SHORT:  return att_get_impl<std::vector<int16_t>>(ncid, varid, name, type, len);
      case NC_USHORT: return att_get_impl<std::vector<uint16_t>>(ncid, varid, name, type, len);
      case NC_INT:    return att_get_impl<std::vector<int32_t>>(ncid, varid, name, type, len);
      case NC_UINT:   return att_get_impl<std::vector<uint32_t>>(ncid, varid, name, type, len);
      case NC_INT64:  return att_get_impl<std::vector<int64_t>>(ncid, varid, name, type, len);
      case NC_UINT64: return att_get_impl<std::vector<uint64_t>>(ncid, varid, name, type, len);
      case NC_FLOAT:  return att_get_impl<std::vector<float>>(ncid, varid, name, type, len);
      case NC_DOUBLE: return att_get_impl<std::vector<double>>(ncid, varid, name, type, len);
      case NC_CHAR:   return att_get_impl<std::string>(ncid, varid, name, type, len); // char array is still a scalar string
      case NC_STRING: return att_get_impl<std::vector<std::string>>(ncid, varid, name, type, len);
      default:        throw error{"Unexpected type for attribute", NC_NOERR, ncid, varid, name};
      }
    }
    else
    {
      switch (type)
      {
      case NC_BYTE:   return att_get_impl<int8_t>(ncid, varid, name, type, len);
      case NC_UBYTE:  return att_get_impl<uint8_t>(ncid, varid, name, type, len);
      case NC_SHORT:  return att_get_impl<int16_t>(ncid, varid, name, type, len);
      case NC_USHORT: return att_get_impl<uint16_t>(ncid, varid, name, type, len);
      case NC_INT:    return att_get_impl<int32_t>(ncid, varid, name, type, len);
      case NC_UINT:   return att_get_impl<uint32_t>(ncid, varid, name, type, len);
      case NC_INT64:  return att_get_impl<int64_t>(ncid, varid, name, type, len);
      case NC_UINT64: return att_get_impl<uint64_t>(ncid, varid, name, type, len);
      case NC_FLOAT:  return att_get_impl<float>(ncid, varid, name, type, len);
      case NC_DOUBLE: return att_get_impl<double>(ncid, varid, name, type, len);
      case NC_CHAR:   return att_get_impl<std::string>(ncid, varid, name, type, len);
      case NC_STRING: return att_get_impl<std::string>(ncid, varid, name, type, len);
      default:        throw error{"Unexpected type for attribute", NC_NOERR, ncid, varid, name};
      }
    }
  }

  // helper functions to write an attribute (array form)
  auto att_set(int ncid, int varid, char const* name, signed char const* data, size_t len) -> void;
  auto att_set(int ncid, int varid, char const* name, unsigned char const* data, size_t len) -> void;
  auto att_set(int ncid, int varid, char const* name, short const* data, size_t len) -> void;
  auto att_set(int ncid, int varid, char const* name, unsigned short const* data, size_t len) -> void;
  auto att_set(int ncid, int varid, char const* name, int const* data, size_t len) -> void;
  auto att_set(int ncid, int varid, char const* name, unsigned int const* data, size_t len) -> void;
  auto att_set(int ncid, int varid, char const* name, long const* data, size_t len) -> void;
  auto att_set(int ncid, int varid, char const* name, unsigned long const* data, size_t len) -> void;
  auto att_set(int ncid, int varid, char const* name, long long const* data, size_t len) -> void;
  auto att_set(int ncid, int varid, char const* name, unsigned long long const* data, size_t len) -> void;
  auto att_set(int ncid, int varid, char const* name, float const* data, size_t len) -> void;
  auto att_set(int ncid, int varid, char const* name, double const* data, size_t len) -> void;
  auto att_set(int ncid, int varid, char const* name, char const** data, size_t len) -> void;
  auto att_set(int ncid, int varid, char const* name, std::string const* data, size_t len) -> void;

  // helper functions to write an attribute (scalar form)
  // only the bool and string types require special implementations
  inline auto att_set(int ncid, int varid, char const* name, signed char val) -> void { att_set(ncid, varid, name, &val, 1); }
  inline auto att_set(int ncid, int varid, char const* name, unsigned char val) -> void { att_set(ncid, varid, name, &val, 1); }
  inline auto att_set(int ncid, int varid, char const* name, short val) -> void { att_set(ncid, varid, name, &val, 1); }
  inline auto att_set(int ncid, int varid, char const* name, unsigned short val) -> void { att_set(ncid, varid, name, &val, 1); }
  inline auto att_set(int ncid, int varid, char const* name, int val) -> void { att_set(ncid, varid, name, &val, 1); }
  inline auto att_set(int ncid, int varid, char const* name, unsigned int val) -> void { att_set(ncid, varid, name, &val, 1); }
  inline auto att_set(int ncid, int varid, char const* name, long val) -> void { att_set(ncid, varid, name, &val, 1); }
  inline auto att_set(int ncid, int varid, char const* name, unsigned long val) -> void { att_set(ncid, varid, name, &val, 1); }
  inline auto att_set(int ncid, int varid, char const* name, long long val) -> void { att_set(ncid, varid, name, &val, 1); }
  inline auto att_set(int ncid, int varid, char const* name, unsigned long long val) -> void { att_set(ncid, varid, name, &val, 1); }
  inline auto att_set(int ncid, int varid, char const* name, float val) -> void { att_set(ncid, varid, name, &val, 1); }
  inline auto att_set(int ncid, int varid, char const* name, double val) -> void { att_set(ncid, varid, name, &val, 1); }
  auto att_set(int ncid, int varid, char const* name, char const* data) -> void;
  auto att_set(int ncid, int varid, char const* name, std::string const& data) -> void;

  auto var_exists(int ncid, char const* name) -> bool;

  // scalar variable access
  template <typename T>
  auto var_get_impl(int ncid, int varid) -> std::optional<T>
  {
    std::optional<T> val{std::in_place};
    if constexpr (std::is_same_v<T, std::string>)
    {
      char *ptr;
      wrap_nc_get_var(ncid, varid, &ptr);
      val->assign(ptr);
      nc_free_string(1, &ptr);
    }
    else
      wrap_nc_get_var(ncid, varid, &(*val));
    return val;
  }
  template <typename T>
  auto var_get(int ncid, char const* name) -> std::optional<T>
  {
    int varid;
    if (auto status = nc_inq_varid(ncid, name, &varid))
    {
      if (status != NC_ENOTVAR)
        throw error{"Failed to read variable", status, ncid, NC_GLOBAL, name};
      return std::nullopt;
    }

    nc_type type; int ndims;
    if (auto status = nc_inq_var(ncid, varid, nullptr, &type, &ndims, nullptr, nullptr))
      throw error{"Failed to read variable", status, ncid, NC_GLOBAL, name};
    if (ndims != 0)
      throw error{"Failed to read variable", "Scalar variable expected", ncid, NC_GLOBAL, name};

    return var_get_impl<T>(ncid, varid);
  }
  template <>
  inline auto var_get(int ncid, char const* name) -> std::optional<metadata_scalar>
  {
    int varid;
    if (auto status = nc_inq_varid(ncid, name, &varid))
    {
      if (status != NC_ENOTVAR)
        throw error{"Failed to read variable", status, ncid, NC_GLOBAL, name};
      return std::nullopt;
    }

    nc_type type; int ndims;
    if (auto status = nc_inq_var(ncid, varid, nullptr, &type, &ndims, nullptr, nullptr))
      throw error{"Failed to read variable", status, ncid, NC_GLOBAL, name};
    if (ndims != 0)
      throw error{"Failed to read variable", "Scalar variable expected", ncid, NC_GLOBAL, name};

    switch (type)
    {
    case NC_BYTE:   return var_get_impl<int8_t>(ncid, varid);
    case NC_UBYTE:  return var_get_impl<uint8_t>(ncid, varid);
    case NC_SHORT:  return var_get_impl<int16_t>(ncid, varid);
    case NC_USHORT: return var_get_impl<uint16_t>(ncid, varid);
    case NC_INT:    return var_get_impl<int32_t>(ncid, varid);
    case NC_UINT:   return var_get_impl<uint32_t>(ncid, varid);
    case NC_INT64:  return var_get_impl<int64_t>(ncid, varid);
    case NC_UINT64: return var_get_impl<uint64_t>(ncid, varid);
    case NC_FLOAT:  return var_get_impl<float>(ncid, varid);
    case NC_DOUBLE: return var_get_impl<double>(ncid, varid);
    case NC_STRING: return var_get_impl<std::string>(ncid, varid);
    default:        throw error{"Unexpected type for variable", NC_NOERR, ncid, varid};
    }
  }

  // 1d variable access
  template <typename T>
  auto var_get_impl(int ncid, int varid, size_t len) -> std::optional<T>
  {
    std::optional<T> val{std::in_place, len};
    if constexpr (std::is_same_v<T, std::vector<std::string>>)
    {
      auto ptrs = std::make_unique<char*[]>(len);
      wrap_nc_get_var(ncid, varid, ptrs.get());
      for (size_t i = 0; i < len; ++i)
        (*val)[i].assign(ptrs[i]);
      nc_free_string(len, ptrs.get());
    }
    else
      wrap_nc_get_var(ncid, varid, val->data());
    return val;
  }
  template <typename T>
  auto var_get(int ncid, char const* name, int dimid) -> std::optional<T>
  {
    int varid;
    if (auto status = nc_inq_varid(ncid, name, &varid))
    {
      if (status != NC_ENOTVAR)
        throw error{"Failed to read variable", status, ncid, NC_GLOBAL, name};
      return std::nullopt;
    }

    nc_type type; int ndims;
    if (auto status = nc_inq_var(ncid, varid, nullptr, &type, &ndims, nullptr, nullptr))
      throw error{"Failed to read variable", status, ncid, NC_GLOBAL, name};
    if (ndims != 1)
      throw error{"Failed to read variable", "1D variable expected", ncid, NC_GLOBAL, name};

    int found_dimid;
    if (auto status = nc_inq_vardimid(ncid, varid, &found_dimid))
      throw error{"Failed to read variable", status, ncid, NC_GLOBAL, name};
    if (found_dimid != dimid)
      throw error{"Failed to read variable", "Unexpected variable dimension", ncid, NC_GLOBAL, name};

    auto len = wrap_nc_inq_dimlen(ncid, dimid);

    return var_get_impl<T>(ncid, varid, len);
  }
  template <>
  inline auto var_get(int ncid, char const* name, int dimid) -> std::optional<metadata_vector>
  {
    int varid;
    if (auto status = nc_inq_varid(ncid, name, &varid))
    {
      if (status != NC_ENOTVAR)
        throw error{"Failed to read variable", status, ncid, NC_GLOBAL, name};
      return std::nullopt;
    }

    nc_type type; int ndims;
    if (auto status = nc_inq_var(ncid, varid, nullptr, &type, &ndims, nullptr, nullptr))
      throw error{"Failed to read variable", status, ncid, NC_GLOBAL, name};
    if (ndims != 1)
      throw error{"Failed to read variable", "1D variable expected", ncid, NC_GLOBAL, name};

    int found_dimid;
    if (auto status = nc_inq_vardimid(ncid, varid, &found_dimid))
      throw error{"Failed to read variable", status, ncid, NC_GLOBAL, name};
    if (found_dimid != dimid)
      throw error{"Failed to read variable", "Unexpected variable dimension", ncid, NC_GLOBAL, name};

    auto len = wrap_nc_inq_dimlen(ncid, dimid);

    switch (type)
    {
    case NC_BYTE:   return var_get_impl<std::vector<int8_t>>(ncid, varid, len);
    case NC_UBYTE:  return var_get_impl<std::vector<uint8_t>>(ncid, varid, len);
    case NC_SHORT:  return var_get_impl<std::vector<int16_t>>(ncid, varid, len);
    case NC_USHORT: return var_get_impl<std::vector<uint16_t>>(ncid, varid, len);
    case NC_INT:    return var_get_impl<std::vector<int32_t>>(ncid, varid, len);
    case NC_UINT:   return var_get_impl<std::vector<uint32_t>>(ncid, varid, len);
    case NC_INT64:  return var_get_impl<std::vector<int64_t>>(ncid, varid, len);
    case NC_UINT64: return var_get_impl<std::vector<uint64_t>>(ncid, varid, len);
    case NC_FLOAT:  return var_get_impl<std::vector<float>>(ncid, varid, len);
    case NC_DOUBLE: return var_get_impl<std::vector<double>>(ncid, varid, len);
    case NC_STRING: return var_get_impl<std::vector<std::string>>(ncid, varid, len);
    default:        throw error{"Unexpected type for variable", NC_NOERR, ncid, varid};
    }
  }

  // scalar entry from 1d variable access
  template <typename T>
  auto var_get1_impl(int ncid, int varid, size_t index) -> std::optional<T>
  {
    std::optional<T> val{std::in_place};
    if constexpr (std::is_same_v<T, std::string>)
    {
      char *ptr;
      wrap_nc_get_var1(ncid, varid, &index, &ptr);
      val->assign(ptr);
      nc_free_string(1, &ptr);
    }
    else
      wrap_nc_get_var1(ncid, varid, &index, &(*val));
    return val;
  }
  template <typename T>
  auto var_get1(int ncid, char const* name, int dimid, size_t index) -> std::optional<T>
  {
    int varid;
    if (auto status = nc_inq_varid(ncid, name, &varid))
    {
      if (status != NC_ENOTVAR)
        throw error{"Failed to read variable", status, ncid, NC_GLOBAL, name};
      return std::nullopt;
    }

    int ndims;
    if (auto status = nc_inq_varndims(ncid, varid, &ndims))
      throw error{"Failed to read variable", status, ncid, NC_GLOBAL, name};
    if (ndims != 1)
      throw error{"Failed to read variable", "1D variable expected", ncid, NC_GLOBAL, name};

    int found_dimid;
    if (auto status = nc_inq_vardimid(ncid, varid, &found_dimid))
      throw error{"Failed to read variable", status, ncid, NC_GLOBAL, name};
    if (found_dimid != dimid)
      throw error{"Failed to read variable", "Unexpected variable dimension", ncid, NC_GLOBAL, name};

    return var_get1_impl<T>(ncid, varid, index);
  }
  template <>
  inline auto var_get1(int ncid, char const* name, int dimid, size_t index) -> std::optional<metadata_scalar>
  {
    int varid;
    if (auto status = nc_inq_varid(ncid, name, &varid))
    {
      if (status != NC_ENOTVAR)
        throw error{"Failed to read variable", status, ncid, NC_GLOBAL, name};
      return std::nullopt;
    }

    nc_type type; int ndims;
    if (auto status = nc_inq_var(ncid, varid, nullptr, &type, &ndims, nullptr, nullptr))
      throw error{"Failed to read variable", status, ncid, NC_GLOBAL, name};
    if (ndims != 1)
      throw error{"Failed to read variable", "1D variable expected", ncid, NC_GLOBAL, name};

    int found_dimid;
    if (auto status = nc_inq_vardimid(ncid, varid, &found_dimid))
      throw error{"Failed to read variable", status, ncid, NC_GLOBAL, name};
    if (found_dimid != dimid)
      throw error{"Failed to read variable", "Unexpected variable dimension", ncid, NC_GLOBAL, name};

    switch (type)
    {
    case NC_BYTE:   return var_get1_impl<int8_t>(ncid, varid, index);
    case NC_UBYTE:  return var_get1_impl<uint8_t>(ncid, varid, index);
    case NC_SHORT:  return var_get1_impl<int16_t>(ncid, varid, index);
    case NC_USHORT: return var_get1_impl<uint16_t>(ncid, varid, index);
    case NC_INT:    return var_get1_impl<int32_t>(ncid, varid, index);
    case NC_UINT:   return var_get1_impl<uint32_t>(ncid, varid, index);
    case NC_INT64:  return var_get1_impl<int64_t>(ncid, varid, index);
    case NC_UINT64: return var_get1_impl<uint64_t>(ncid, varid, index);
    case NC_FLOAT:  return var_get1_impl<float>(ncid, varid, index);
    case NC_DOUBLE: return var_get1_impl<double>(ncid, varid, index);
    case NC_STRING: return var_get1_impl<std::string>(ncid, varid, index);
    default:        throw error{"Unexpected type for variable", NC_NOERR, ncid, varid};
    }
  }

  /* returns a pair of the variable handle (varid) and a boolean indicating whether this call created the variable.
   * this allows callers to detect variable creation and write associated attributes */
  template <typename T>
  auto var_set(int ncid, char const* name, T const& data) -> std::pair<int, bool>
  {
    static_assert(!is_specialization<T, std::vector>::value, "Scalar var_set called with vector");

    int varid = NC_GLOBAL;
    bool isnew;
    if (auto status = nc_inq_varid(ncid, name, &varid))
    {
      if (status != NC_ENOTVAR)
        throw error{"Failed to write variable", status, ncid, NC_GLOBAL, name};
      if (auto status = nc_def_var(ncid, name, native_to_nc_type<T>(), 0, nullptr, &varid))
        throw error{"Failed to create variable", status, ncid, NC_GLOBAL, name};
      isnew = true;
    }
    else
    {
      int ndims;
      if (auto status = nc_inq_varndims(ncid, varid, &ndims))
        throw error{"Failed to read variable", status, ncid, NC_GLOBAL, name};
      if (ndims != 0)
        throw error{"Failed to read variable", "1D variable expected", ncid, NC_GLOBAL, name};
      isnew = false;
    }

    if constexpr (std::is_same_v<T, std::string>)
    {
      auto ptr = data.data();
      wrap_nc_put_var(ncid, varid, &ptr);
    }
    else
      wrap_nc_put_var(ncid, varid, &data);

    return {varid, isnew};
  }
  template <typename T>
  auto var_set(int ncid, char const* name, int dimid, T const& data) -> std::pair<int, bool>
  {
    static_assert(is_specialization<T, std::vector>::value, "Vector var_set called with scalar");

    int varid = NC_GLOBAL;
    bool isnew;
    if (auto status = nc_inq_varid(ncid, name, &varid))
    {
      if (status != NC_ENOTVAR)
        throw error{"Failed to write variable", status, ncid, NC_GLOBAL, name};
      if (auto status = nc_def_var(ncid, name, native_to_nc_type<typename T::value_type>(), 1, &dimid, &varid))
        throw error{"Failed to create variable", status, ncid, NC_GLOBAL, name};
      isnew = true;
    }
    else
      isnew = false;

    if (data.size() != wrap_nc_inq_dimlen(ncid, dimid))
      throw error{"Array size mismatch", NC_NOERR, ncid, varid, name};

    if constexpr (std::is_same_v<T, std::vector<std::string>>)
    {
      auto ptrs = std::make_unique<char const*[]>(data.size());
      for (size_t i = 0; i < data.size(); ++i)
        ptrs[i] = data[i].data();
      wrap_nc_put_var(ncid, varid, ptrs.get());
    }
    else
      wrap_nc_put_var(ncid, varid, data.data());

    return {varid, isnew};
  }
  template <typename T>
  auto var_set1(int ncid, char const* name, int dimid, size_t index, T const& data) -> std::pair<int, bool>
  {
    int varid = NC_GLOBAL;
    bool isnew;
    if (auto status = nc_inq_varid(ncid, name, &varid))
    {
      if (status != NC_ENOTVAR)
        throw error{"Failed to write variable", status, ncid, NC_GLOBAL, name};
      if (auto status = nc_def_var(ncid, name, native_to_nc_type<T>(), 1, &dimid, &varid))
        throw error{"Failed to create variable", status, ncid, NC_GLOBAL, name};
      isnew = true;
    }
    else
      isnew = false;

    if constexpr (std::is_same_v<T, std::string>)
    {
      auto ptr = data.data();
      wrap_nc_put_var(ncid, varid, &ptr);
    }
    else
      wrap_nc_put_var(ncid, varid, &data);

    return {varid, isnew};
  }
}
