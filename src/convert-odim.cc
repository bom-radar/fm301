/*---------------------------------------------------------------------------------------------------------------------
 * FM301 C++ API and Toolkit
 *
 * Copyright 2021 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *-------------------------------------------------------------------------------------------------------------------*/
#include "fm301.h"
#include "ncutils.h"

#include <hdf5.h>
#include <algorithm>
#include <cstring>
#include <ctime>
#include <iostream>
#include <string>
#include <tuple>

using namespace fm301;

using namespace std::literals;

constexpr auto speed_of_light = 300000000.0;

auto odim_quantity_map = std::array<std::pair<char const*, quantity_traits>, 67>
{{
    { "TH", standard_quantity_traits(standard_quantity::dbth) }
  , { "TV", standard_quantity_traits(standard_quantity::dbtv) }
  , { "DBZH", standard_quantity_traits(standard_quantity::dbzh) }
  , { "DBZV", standard_quantity_traits(standard_quantity::dbzv) }
  , { "ZDR", standard_quantity_traits(standard_quantity::zdr) }
  , { "UZDR", { "UZDR", "radar_differential_reflectivity_hv", "Log differential reflectivity H/V (uncorrected)", "dB" } }
  , { "RHOHV", standard_quantity_traits(standard_quantity::rhohv) }
  , { "URHOHV", { "URHOHV", "radar_correlation_coefficient_hv", "Correlation coefficient HV (uncorrected)", "1" } }
  , { "LDR", standard_quantity_traits(standard_quantity::ldr) }
  , { "LDR", { "ULDR", "radar_linear_depolarization_ratio", "Log-linear depolarization ratio HV (uncorrected)", "dB" } }
  , { "PHIDP", standard_quantity_traits(standard_quantity::phidp) }
  , { "UPHIDP", { "UPHIDP", "radar_differential_phase_hv", "Differential phase HV (uncorrected)", "degree" } }
  , { "PIA", { "PIA", nullptr, "Path integrated attenuation", "dB" } }
  , { "KDP", standard_quantity_traits(standard_quantity::kdp) }
  , { "UKDP", { "UKDP", "radar_specific_differential_phase_hv", "Specific differential phase HV (uncorrected)", "degree km-1" } }
  , { "SQIH", standard_quantity_traits(standard_quantity::ncph) }
  , { "USQIH", { "UNCPH", "radar_normalized_coherent_power_h", "Normalized coherent power co-polar H (uncorrected)", "1" } }
  , { "SQIV", standard_quantity_traits(standard_quantity::ncpv) }
  , { "USQIV", { "UNCPV", "radar_normalized_coherent_power_v", "Normalized coherent power co-polar V (uncorrected)", "1" } }
  , { "SNRH", { "SNRH", nullptr, "Normalized signal-to-noise ratio co-polar H", "1" } } // deprecated ODIM 2.3
  , { "SNRV", { "SNRV", nullptr, "Normalized signal-to-noise ratio co-polar V", "1" } } // deprecated ODIM 2.3
  , { "SNR", standard_quantity_traits(standard_quantity::snr) }
  , { "SNRHC", standard_quantity_traits(standard_quantity::snrhc) }
  , { "SRNHX", standard_quantity_traits(standard_quantity::snrhx) }
  , { "SNRVC", standard_quantity_traits(standard_quantity::snrvc) }
  , { "SNRVX", standard_quantity_traits(standard_quantity::snrvx) }
  , { "CCORH", { "CCORH", nullptr, "Clutter correction H", "dB" } }
  , { "CCORV", { "CCORV", nullptr, "Clutter correction V", "dB" } }
  , { "CPA", { "CPA", nullptr, "Clutter phase alignment", "1" } }
  , { "RATE", standard_quantity_traits(standard_quantity::rr) }
  , { "URATE", { "URR", "radar_estimated_precipitation_rate", "Rain rate (uncorrected)", "mm/hr" } }
  , { "POR", { "POR", nullptr, "Probability of rain", "1" } }
  , { "HI", { "HI", nullptr, "Hail intensity", "dBZ" } }
  , { "HP", { "HP", nullptr, "Hail probability", "1" } } // deprecated ODIM 2.3
  , { "POH", { "POH", nullptr, "Probability of hail", "1" } }
  , { "POSH", { "POSH", nullptr, "Probability of severe hail", "1" } }
  , { "MESH", { "MESH", nullptr, "Maximum expected severe hail size", "cm" } }
  , { "ACRR", { "ACRR", nullptr, "Accumulated precipitation", "mm" } }
  , { "HGHT", { "HGHT", nullptr, "Height above mean sea level", "km" } }
  , { "VIL", { "VIL", nullptr, "Vertical integrated liquid water", "kg m-2" } }
  , { "VRADH", standard_quantity_traits(standard_quantity::vradh) }
  , { "UVRADH", { "UVRADH", "radial_velocity_of_scatterers_away_from_instrument_h", "Radial velocity of scatterers away from instrument H (uncorrected)", "m s-1" } }
  , { "VRADV", standard_quantity_traits(standard_quantity::vradv) }
  , { "UVRADV", { "UVRADH", "radial_velocity_of_scatterers_away_from_instrument_v", "Radial velocity of scatterers away from instrument V (uncorrected)", "m s-1" } }
  , { "VRADDH", { "VRADDH", "radial_velocity_of_scatterers_away_from_instrument_h", "Radial velocity of scatterers away from instrument H (dealiased)", "m s-1" } }
  , { "VRADDV", { "VRADDV", "radial_velocity_of_scatterers_away_from_instrument_v", "Radial velocity of scatterers away from instrument V (dealiased)", "m s-1" } }
  , { "WRADH", standard_quantity_traits(standard_quantity::wradh) }
  , { "UWRADH", { "UWRADH", "radar_doppler_spectrum_width_h", "Doppler spectrum width H (uncorrected)", "m s-1" } }
  , { "WRADV", standard_quantity_traits(standard_quantity::wradv) }
  , { "UWRADV", { "UWRADV", "radar_doppler_spectrum_width_v", "Doppler spectrum width V (uncorrected)", "m s-1" } }
  , { "UWND", { "UWND", nullptr, "Component of wind in x-direction", "m s-1" } }
  , { "VWND", { "VWND", nullptr, "Component of wind in y-direction", "m s-1" } }
  , { "RSHR", { "RSHR", nullptr, "Radial shear", "m s-1 km-1" } }
  , { "ASHR", { "ASHR", nullptr, "Azimuthal shear", "m s-1 km-1" } }
  , { "CSHR", { "CSHR", nullptr, "Range-azimuthal shear", "m s-1 km-1" } }
  , { "ESHR", { "ESHR", nullptr, "Elevation shear", "m s-1 km-1" } }
  , { "OSHR", { "OSHR", nullptr, "Range-elevation shear", "m s-1 km-1" } }
  , { "HSHR", { "HSHR", nullptr, "Horizontal shear", "m s-1 km-1" } }
  , { "VSHR", { "VSHR", nullptr, "Vertical shear", "m s-1 km-1" } }
  , { "TSHR", { "TSHR", nullptr, "Three-dimensional shear", "m s-1 km-1" } }
  , { "PSPH", { "PSPH", nullptr, "Power spectrum peak H", "dBm" } }
  , { "PSPV", { "PSPV", nullptr, "Power spectrum peak V", "dBm" } }
  , { "UPSPH", { "UPSPH", nullptr, "Power spectrum peak H (uncorrected)", "dBm" } }
  , { "UPSPV", { "UPSPV", nullptr, "Power spectrum peak V (uncorrected)", "dBm" } }
  , { "BRDR", { "BRDR", nullptr, "Border indicator", "1" } }
  , { "QIND", { "QIND", nullptr, "Quality indicator", "1" } }
  , { "CLASS", standard_quantity_traits(standard_quantity::rec) }
}};

static auto odim_quantity_traits(std::string const& name) -> quantity_traits const*
{
  for (auto& traits : odim_quantity_map)
    if (traits.first == name)
      return &traits.second;
  return nullptr;
}

static auto odim_quantity_from_varname(std::string const& name) -> char const*
{
  for (auto& traits : odim_quantity_map)
    if (traits.second.name == name)
      return traits.first;
  return name.c_str();
}

// raii handle for hdf handle type (hid_t)
struct hdf_handle
{
  hid_t id;
  hdf_handle() noexcept : id{-1} { }
  explicit hdf_handle(hid_t id) noexcept : id{id} { }
  hdf_handle(hdf_handle const& rhs) : id{rhs.id} { if (id > 0) H5Iinc_ref(id); }
  hdf_handle(hdf_handle&& rhs) noexcept : id(rhs.id) { rhs.id = -1; }
  auto operator=(hdf_handle&& rhs) noexcept -> hdf_handle& { std::swap(id, rhs.id); return *this; }
  ~hdf_handle() { if (id > 0) H5Idec_ref(id); }
  operator hid_t() const noexcept { return id; }
  operator bool() const noexcept  { return id > 0; }
};

// map between a native type and the corresponding H5T_NATIVE_xxx macro
/* Somewhat astoundingly, these H5T_xxx macros are not constants, but are actually hidden calls to call H5open!
 * This means we cannot implement this functionality using constexpr. */
template <typename T> hid_t hdf_native_type();
template <> hid_t hdf_native_type<int8_t>()    { return H5T_NATIVE_INT8; }
template <> hid_t hdf_native_type<uint8_t>()   { return H5T_NATIVE_UINT8; }
template <> hid_t hdf_native_type<int16_t>()   { return H5T_NATIVE_INT16; }
template <> hid_t hdf_native_type<uint16_t>()  { return H5T_NATIVE_UINT16; }
template <> hid_t hdf_native_type<int32_t>()   { return H5T_NATIVE_INT32; }
template <> hid_t hdf_native_type<uint32_t>()  { return H5T_NATIVE_UINT32; }
template <> hid_t hdf_native_type<int64_t>()   { return H5T_NATIVE_INT64; }
template <> hid_t hdf_native_type<uint64_t>()  { return H5T_NATIVE_UINT64; }
template <> hid_t hdf_native_type<float>()     { return H5T_NATIVE_FLOAT; }
template <> hid_t hdf_native_type<double>()    { return H5T_NATIVE_DOUBLE; }

// map between a native type and the corresponding H5T_class_t constant
template <typename T> constexpr H5T_class_t hdf_type_class();
template <> constexpr H5T_class_t hdf_type_class<int8_t>()       { return H5T_INTEGER; }
template <> constexpr H5T_class_t hdf_type_class<uint8_t>()      { return H5T_INTEGER; }
template <> constexpr H5T_class_t hdf_type_class<int16_t>()      { return H5T_INTEGER; }
template <> constexpr H5T_class_t hdf_type_class<uint16_t>()     { return H5T_INTEGER; }
template <> constexpr H5T_class_t hdf_type_class<int32_t>()      { return H5T_INTEGER; }
template <> constexpr H5T_class_t hdf_type_class<uint32_t>()     { return H5T_INTEGER; }
template <> constexpr H5T_class_t hdf_type_class<int64_t>()      { return H5T_INTEGER; }
template <> constexpr H5T_class_t hdf_type_class<uint64_t>()     { return H5T_INTEGER; }
template <> constexpr H5T_class_t hdf_type_class<float>()        { return H5T_FLOAT; }
template <> constexpr H5T_class_t hdf_type_class<double>()       { return H5T_FLOAT; }
template <> constexpr H5T_class_t hdf_type_class<std::string>()  { return H5T_STRING; }

static auto hdf_storage_type(data_type type) -> hid_t
{
  switch (type)
  {
  case data_type::i8:
    return H5T_STD_I8LE;
  case data_type::u8:
    return H5T_STD_U8LE;
  case data_type::i16:
    return H5T_STD_I16LE;
  case data_type::u16:
    return H5T_STD_U16LE;
  case data_type::i32:
    return H5T_STD_I32LE;
  case data_type::u32:
    return H5T_STD_U32LE;
  case data_type::i64:
    return H5T_STD_I64LE;
  case data_type::u64:
    return H5T_STD_U64LE;
  case data_type::f32:
    return H5T_IEEE_F32LE;
  case data_type::f64:
    return H5T_IEEE_F64LE;
  default:
    throw std::runtime_error{"Unexpected type"};
  }
}

static auto hdf_group_exists(hid_t loc_id, char const* name) -> bool
{
  auto ret = H5Lexists(loc_id, name, H5P_DEFAULT);
  if (ret < 0)
    throw std::runtime_error{"Failed to check if "s + name + " group exists"};
  return ret != 0;
}

static auto hdf_group_open(hid_t loc_id, char const* name) -> hdf_handle
{
  auto ret = H5Gopen(loc_id, name, H5P_DEFAULT);
  if (ret < 0)
    throw std::runtime_error{"Failed to open group "s + name};
  return hdf_handle{ret};
}

static auto hdf_group_create(hid_t loc_id, char const* name) -> hdf_handle
{
  auto ret = H5Gcreate(loc_id, name, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
  if (ret < 0)
    throw std::runtime_error{"Failed to create group "s + name};
  return hdf_handle{ret};
}

static auto hdf_dataset_open(hid_t loc_id, char const* name) -> hdf_handle
{
  auto ret = H5Dopen(loc_id, name, H5P_DEFAULT);
  if (ret < 0)
    throw std::runtime_error{"Failed to open dataset "s + name};
  return hdf_handle{ret};
}

static auto hdf_dataset_type(hid_t loc_id) -> data_type
{
  auto htype = hdf_handle{H5Dget_type(loc_id)};
  if (!htype)
    throw std::runtime_error{"Failed to read dataset type"};

  auto type = H5Tget_class(htype);
  auto size = H5Tget_size(htype);
  if (type == H5T_INTEGER)
  {
    auto sign = H5Tget_sign(htype) == H5T_SGN_2;
    switch (size)
    {
    case 1:
      return sign ? data_type::i8 : data_type::u8;
    case 2:
      return sign ? data_type::i16 : data_type::u16;
    case 4:
      return sign ? data_type::i32 : data_type::u32;
    case 8:
      return sign ? data_type::i64 : data_type::u64;
    }
  }
  else if (type == H5T_FLOAT)
  {
    switch (size)
    {
    case 4:
      return data_type::f32;
    case 8:
      return data_type::f64;
    }
  }
  throw std::runtime_error{"Unexpected dataset type"};
}

static auto hdf_dataset_size(hid_t loc_id) -> size_t
{
  auto hspace = hdf_handle{H5Dget_space(loc_id)};
  if (!hspace)
    throw std::runtime_error{"Failed to read dataset size"};
  auto size = H5Sget_simple_extent_npoints(hspace);
  if (size < 0)
    throw std::runtime_error{"Failed to read dataset size"};
  return size;
}

static auto hdf_attribute_exists(hid_t loc_id, char const* name) -> bool
{
  auto ret = H5Aexists(loc_id, name);
  if (ret < 0)
    throw std::runtime_error{"Failed to check if attribute exists"};
  return ret != 0;
}

static auto hdf_attribute_open(hid_t loc_id, char const* name) -> hdf_handle
{
  auto ret = H5Aopen(loc_id, name, H5P_DEFAULT);
  if (ret < 0)
    throw std::runtime_error{"Failed to open group "s + name};
  return hdf_handle{ret};
}

template <typename T>
static auto hdf_attribute_read(hid_t loc_id, char const* name) -> T
{
  auto hattr = hdf_attribute_open(loc_id, name);

  auto hspace = hdf_handle{H5Aget_space(hattr)};
  if (!hspace)
    throw std::runtime_error{"Failed to read attribute "s + name + " dataspace"};
  auto size = H5Sget_simple_extent_npoints(hspace);
  if (size < 0)
    throw std::runtime_error{"Failed to read attribute "s + name + " dataspace size"};
  if constexpr (!is_specialization<T, std::vector>::value)
    if (size != 1)
      throw std::runtime_error{"Failed to read attribute "s + name + ", expected scalar"};

  auto htype = hdf_handle{H5Aget_type(hattr)};
  if (!htype)
    throw std::runtime_error{"Failed to read attribute "s + name + " type"};
  if constexpr (is_specialization<T, std::vector>::value)
  {
    if (H5Tget_class(htype) != hdf_type_class<typename T::value_type>())
      throw std::runtime_error{"Failed to read attribute "s + name + ", unexpected type"};
  }
  else
  {
    if (H5Tget_class(htype) != hdf_type_class<T>())
      throw std::runtime_error{"Failed to read attribute "s + name + ", unexpected type"};
  }

  if constexpr (std::is_same_v<T, std::string>)
  {
    auto str_size = H5Tget_size(htype);
    T val(str_size - 1, '\0'); // odim size includes the null terminator
    if (H5Aread(hattr, htype, val.data()) < 0)
      throw std::runtime_error{"Failed to read attribute "s + name};
    return val;
  }
  else if constexpr (is_specialization<T, std::vector>::value)
  {
    T val(size);
    if (H5Aread(hattr, hdf_native_type<typename T::value_type>(), val.data()) < 0)
      throw std::runtime_error{"Failed to read attribute "s + name};
    return val;
  }
  else
  {
    T val;
    if (H5Aread(hattr, hdf_native_type<T>(), &val) < 0)
      throw std::runtime_error{"Failed to read attribute "s + name};
    return val;
  }
}

static auto hdf_attribute_create(hid_t loc_id, char const* name, bool val) -> void
{
  auto type = hdf_handle{H5Tcopy(H5T_C_S1)};
  if (   !type
      || H5Tset_size(type, val ? 5 : 6) < 0
      || H5Tset_strpad(type, H5T_STR_NULLTERM) < 0)
    throw std::runtime_error{"Failed to write attribute "s + name};
  auto space = hdf_handle{H5Screate(H5S_SCALAR)};
  if (!space)
    throw std::runtime_error{"Failed to write attribute "s + name};
  auto attribute = hdf_handle{H5Acreate(loc_id, name, type, space, H5P_DEFAULT, H5P_DEFAULT)};
  if (!attribute)
    throw std::runtime_error{"Failed to write attribute "s + name};
  if (H5Awrite(attribute, type, val ? "True" : "False") < 0)
    throw std::runtime_error{"Failed to write attribute "s + name};
}

static auto hdf_attribute_create(hid_t loc_id, char const* name, std::string const& val) -> void
{
  auto type = hdf_handle{H5Tcopy(H5T_C_S1)};
  if (   !type
      || H5Tset_size(type, val.size() + 1) < 0
      || H5Tset_strpad(type, H5T_STR_NULLTERM) < 0)
    throw std::runtime_error{"Failed to write attribute "s + name};
  auto space = hdf_handle{H5Screate(H5S_SCALAR)};
  if (!space)
    throw std::runtime_error{"Failed to write attribute "s + name};
  auto attribute = hdf_handle{H5Acreate(loc_id, name, type, space, H5P_DEFAULT, H5P_DEFAULT)};
  if (!attribute)
    throw std::runtime_error{"Failed to write attribute "s + name};
  if (H5Awrite(attribute, type, val.c_str()) < 0)
    throw std::runtime_error{"Failed to write attribute "s + name};
}

static auto hdf_attribute_create(hid_t loc_id, char const* name, char const* val) -> void
{
  auto len = std::strlen(val);
  auto type = hdf_handle{H5Tcopy(H5T_C_S1)};
  if (   !type
      || H5Tset_size(type, len + 1) < 0
      || H5Tset_strpad(type, H5T_STR_NULLTERM) < 0)
    throw std::runtime_error{"Failed to write attribute "s + name};
  auto space = hdf_handle{H5Screate(H5S_SCALAR)};
  if (!space)
    throw std::runtime_error{"Failed to write attribute "s + name};
  auto attribute = hdf_handle{H5Acreate(loc_id, name, type, space, H5P_DEFAULT, H5P_DEFAULT)};
  if (!attribute)
    throw std::runtime_error{"Failed to write attribute "s + name};
  if (H5Awrite(attribute, type, val) < 0)
    throw std::runtime_error{"Failed to write attribute "s + name};
}

static auto hdf_attribute_create(hid_t loc_id, char const* name, double val) -> void
{
  auto space = hdf_handle{H5Screate(H5S_SCALAR)};
  if (!space)
    throw std::runtime_error{"Failed to write attribute "s + name};
  auto attribute = hdf_handle{H5Acreate(loc_id, name, H5T_IEEE_F64LE, space, H5P_DEFAULT, H5P_DEFAULT)};
  if (!attribute)
    throw std::runtime_error{"Failed to write attribute "s + name};
  if (H5Awrite(attribute, H5T_NATIVE_DOUBLE, &val) < 0)
    throw std::runtime_error{"Failed to write attribute "s + name};
}

static auto hdf_attribute_create(hid_t loc_id, char const* name, long val) -> void
{
  auto space = hdf_handle{H5Screate(H5S_SCALAR)};
  if (!space)
    throw std::runtime_error{"Failed to write attribute "s + name};
  auto attribute = hdf_handle{H5Acreate(loc_id, name, H5T_STD_I64LE, space, H5P_DEFAULT, H5P_DEFAULT)};
  if (!attribute)
    throw std::runtime_error{"Failed to write attribute "s + name};
  if (H5Awrite(attribute, H5T_NATIVE_LONG, &val) < 0)
    throw std::runtime_error{"Failed to write attribute "s + name};
}

static auto hdf_attribute_create(hid_t loc_id, char const* name, std::vector<float> val) -> void
{
  /* we output as doubles even though the user passed us a vector of floats.  this is because
   * ODIM specifies that all floating point attributes are doubles. */
  auto size = hsize_t{val.size()};
  auto space = hdf_handle{H5Screate_simple(1, &size, nullptr)};
  if (!space)
    throw std::runtime_error{"Failed to write attribute "s + name};
  auto attribute = hdf_handle{H5Acreate(loc_id, name, H5T_IEEE_F64LE, space, H5P_DEFAULT, H5P_DEFAULT)};
  if (!attribute)
    throw std::runtime_error{"Failed to write attribute "s + name};
  if (H5Awrite(attribute, H5T_NATIVE_FLOAT, val.data()) < 0)
    throw std::runtime_error{"Failed to write attribute "s + name};
}

static auto hdf_attribute_create(hid_t loc_id, char const* name, std::vector<double> val) -> void
{
  auto size = hsize_t{val.size()};
  auto space = hdf_handle{H5Screate_simple(1, &size, nullptr)};
  if (!space)
    throw std::runtime_error{"Failed to write attribute "s + name};
  auto attribute = hdf_handle{H5Acreate(loc_id, name, H5T_IEEE_F64LE, space, H5P_DEFAULT, H5P_DEFAULT)};
  if (!attribute)
    throw std::runtime_error{"Failed to write attribute "s + name};
  if (H5Awrite(attribute, H5T_NATIVE_DOUBLE, val.data()) < 0)
    throw std::runtime_error{"Failed to write attribute "s + name};
}

template <typename T>
static auto hdf_read_how(std::vector<hdf_handle> const& hows, char const* name) -> std::optional<T>
{
  /* ODIM allows 'how' metadata at inner levels to overwrite values at outer levels, and it doesn't firmly state what
   * level individual 'how' attributes belong at.  this means we need to always search the current how (if any) and all
   * levels above in the hierarchy to find a match. */
  for (auto ihow = hows.rbegin(); ihow != hows.rend(); ++ihow)
    if (*ihow && hdf_attribute_exists(*ihow, name))
      return hdf_attribute_read<T>(*ihow, name);
  return std::nullopt;
}

/* This is a convenience function to detect the largest 'N' in datasetN/dataN/qualityN groups, returns 0 if no matching
 * groups are found.  ODIM groups are indexed from 1, so 0 is safe to use as a 'no groups' flag. */
static auto indexed_group_count(hid_t loc_id, char const* base_name) -> size_t
{
  H5G_info_t info;
  if (H5Gget_info(loc_id, &info) < 0)
    throw std::runtime_error{"Failed to read group info"};
  for (auto i = info.nlinks; i > 0; --i)
  {
    char name[32];
    snprintf(name, 32, "%s%llu", base_name, i);
    if (hdf_group_exists(loc_id, name))
      return i;
  }
  return 0;
}

static auto parse_time_strings(std::string const& date, std::string const& time) -> std::chrono::system_clock::time_point
{
  struct tm tms;
  if (   sscanf(date.c_str(), "%04d%02d%02d", &tms.tm_year, &tms.tm_mon, &tms.tm_mday) != 3
      || sscanf(time.c_str(), "%02d%02d%02d", &tms.tm_hour, &tms.tm_min, &tms.tm_sec) != 3)
    throw std::runtime_error{"Date/time parse error"};
  tms.tm_year -= 1900;
  tms.tm_mon -= 1;
  tms.tm_wday = 0;
  tms.tm_yday = 0;
  tms.tm_isdst = 0;
  return std::chrono::system_clock::from_time_t(timegm(&tms));
}

static auto format_time_strings(std::chrono::system_clock::time_point val) -> std::pair<std::string, std::string>
{
  auto tt = std::chrono::system_clock::to_time_t(val);
  auto tm = std::gmtime(&tt);
  char date_str[32];
  strftime(date_str, 32, "%Y%m%d", tm);
  char time_str[32];
  strftime(time_str, 32, "%H%M%S", tm);
  return std::make_pair<std::string, std::string>(date_str, time_str);
}

template <typename T>
auto reorder_rays_from_odim(int64_t a1gate, bool clockwise, std::vector<T>& data)
{
  if (!clockwise)
  {
    std::reverse(data.begin(), data.end());
    a1gate = data.size() - a1gate - 1;
  }
  std::rotate(data.begin(), data.begin() + a1gate, data.end());
}

template <typename T>
auto reorder_rays_from_odim(int64_t a1gate, bool clockwise, size_t nbins, std::vector<T>& data)
{
  if (!clockwise)
  {
    // need to preserve range ordering of bins, so we can't just use std::reverse on 2d datasets
    // integer division in loop terimination ensures we ignore the middle ray if nrays is odd
    auto nrays = data.size() / nbins;
    for (size_t iray = 0; iray < nrays / 2; ++iray)
      std::swap_ranges(
            data.begin() + (iray * nbins)
          , data.begin() + ((iray+1) * nbins)
          , data.begin() + ((nrays - iray - 1) * nbins));
    a1gate = data.size() - a1gate - 1;
  }
  std::rotate(data.begin(), data.begin() + (a1gate * nbins), data.end());
}

template <typename T>
auto reorder_rays_to_odim(int64_t a1gate, bool clockwise, std::vector<T>& data)
{
  std::rotate(data.rbegin(), data.rbegin() + a1gate, data.rend());
  if (!clockwise)
  {
    std::reverse(data.begin(), data.end());
    a1gate = data.size() - a1gate - 1;
  }
}

template <typename T>
auto reorder_rays_to_odim(int64_t a1gate, bool clockwise, size_t nbins, std::vector<T>& data)
{
  std::rotate(data.rbegin(), data.rbegin() + (a1gate * nbins), data.rend());
  if (!clockwise)
  {
    // need to preserve range ordering of bins, so we can't just use std::reverse on 2d datasets
    // integer division in loop terimination ensures we ignore the middle ray if nrays is odd
    auto nrays = data.size() / nbins;
    for (size_t iray = 0; iray < nrays / 2; ++iray)
      std::swap_ranges(
            data.begin() + (iray * nbins)
          , data.begin() + ((iray+1) * nbins)
          , data.begin() + ((nrays - iray - 1) * nbins));
    a1gate = data.size() - a1gate - 1;
  }
}

// returned durations are offsets from start_time
static auto build_ray_times(
      size_t nrays
    , int64_t a1gate
    , bool clockwise
    , std::chrono::system_clock::time_point start_time
    , std::chrono::system_clock::time_point end_time
    , std::vector<hdf_handle> const& hhows
    ) -> std::vector<std::chrono::duration<double>>
{
  auto times = std::vector<std::chrono::duration<double>>(nrays);
  auto starts = std::vector<double>();
  auto stops = std::vector<double>();

  if (auto val = hdf_read_how<std::vector<double>>(hhows, "startT"))
    starts = std::move(*val);
  else if (auto val = hdf_read_how<std::vector<double>>(hhows, "startazT"))
    starts = std::move(*val);
  else if (auto val = hdf_read_how<std::vector<double>>(hhows, "startelT"))
    starts = std::move(*val);
  if (!starts.empty() && starts.size() != nrays)
    throw std::runtime_error{"Size mismatch on ray start times"};

  if (auto val = hdf_read_how<std::vector<double>>(hhows, "stopT"))
    stops = std::move(*val);
  else if (auto val = hdf_read_how<std::vector<double>>(hhows, "stopazT"))
    stops = std::move(*val);
  else if (auto val = hdf_read_how<std::vector<double>>(hhows, "stopelT"))
    stops = std::move(*val);
  if (!stops.empty() && stops.size() != nrays)
    throw std::runtime_error{"Size mismatch on ray stop times"};

  if (!starts.empty())
    reorder_rays_from_odim(a1gate, clockwise, starts);
  if (!stops.empty())
    reorder_rays_from_odim(a1gate, clockwise, stops);

  auto start_time_t = double(std::chrono::system_clock::to_time_t(start_time));
  if (!starts.empty() && !stops.empty())
  {
    for (size_t i = 0; i < nrays; ++i)
      times[i] = std::chrono::duration<double>(((starts[i] + stops[i]) * 0.5) - start_time_t);
  }
  else if (!starts.empty() && nrays > 1)
  {
    for (size_t i = 0; i < nrays - 1; ++i)
      times[i] = std::chrono::duration<double>(((starts[i] + starts[i+1]) * 0.5) - start_time_t);
    times[nrays-1] = std::chrono::duration<double>(starts[nrays-1] + (starts[nrays-1] - starts[nrays-2]) * 0.5 - start_time_t);
  }
  else if (!stops.empty() && nrays > 1)
  {
    times[0] = std::chrono::duration<double>(stops[0] - (stops[1] - stops[0]) * 0.5 - start_time_t);
    for (size_t i = 1; i < nrays; ++i)
      times[i] = std::chrono::duration<double>(((stops[i-1] + stops[i]) * 0.5) - start_time_t);
  }
  else
  {
    auto delta = std::chrono::duration<float>(end_time - start_time) / nrays;
    auto first = delta * 0.5;
    for (size_t i = 0; i < nrays; ++i)
      times[i] = first + i * delta;
  }

  return times;
}

// there is no doubt a much faster algorithm for this...
static inline auto angle_midpoint(float start, float stop) -> float
{
  auto diff = std::fmod(stop - start + 180.0f, 360.0f) - 180.0f;
  if (diff < -180.0f)
    diff += 360.0f;
  auto mid = std::fmod(start + diff * 0.5f, 360.0f);
  if (mid < 0.0f)
    mid += 360.0f;
  return mid;
}

static auto build_ray_azimuths(
      size_t nrays
    , int64_t a1gate
    , bool clockwise
    , std::vector<hdf_handle> const& hhows
    , float& regular_resolution
    ) -> std::vector<float>
{
  auto starts = std::vector<double>();
  auto stops = std::vector<double>();
  if (auto val = hdf_read_how<std::vector<double>>(hhows, "startazA"))
  {
    starts = std::move(*val);
    if (starts.size() != nrays)
      throw std::runtime_error{"Size mismatch on ray start azimuths"};
  }
  if (auto val = hdf_read_how<std::vector<double>>(hhows, "stopazA"))
  {
    stops = std::move(*val);
    if (stops.size() != nrays)
      throw std::runtime_error{"Size mismatch on ray stop azimuths"};
  }

  auto angles = std::vector<float>(nrays);
  if (!starts.empty() && !stops.empty())
  {
    for (size_t i = 0; i < nrays; ++i)
      angles[i] = angle_midpoint(starts[i], stops[i]);
  }
  else if (!starts.empty() && nrays > 1)
  {
    for (size_t i = 0; i < nrays - 1; ++i)
      angles[i] = angle_midpoint(starts[i], starts[i+1]);
    angles[nrays-1] = angle_midpoint(starts[nrays-1], starts[nrays-1] + (starts[nrays-1] - starts[nrays-2]));
  }
  else if (!stops.empty() && nrays > 1)
  {
    angles[0] = angle_midpoint(stops[0] - (stops[1] - stops[0]), stops[0]);
    for (size_t i = 1; i < nrays; ++i)
      angles[i] = angle_midpoint(stops[i-1], stops[i]);
  }
  else
  {
    auto adelta = 360.0f / nrays;
    auto aoffset = adelta * 0.5;
    if (auto val = hdf_read_how<double>(hhows, "astart"))
      aoffset += *val;
    for (size_t i = 0; i < nrays; ++i)
      angles[i] = aoffset + i * adelta;

    regular_resolution = adelta;
  }

  reorder_rays_from_odim(a1gate, clockwise, angles);

  return angles;
}

static auto build_ray_elevations(
      size_t nrays
    , int64_t a1gate
    , bool clockwise
    , std::vector<hdf_handle> const& hhows
    , double elangle
    ) -> std::vector<float>
{
  auto starts = std::vector<double>();
  auto stops = std::vector<double>();
  if (auto val = hdf_read_how<std::vector<double>>(hhows, "startelA"))
  {
    starts = std::move(*val);
    if (starts.size() != nrays)
      throw std::runtime_error{"Size mismatch on ray start elevations"};
  }
  if (auto val = hdf_read_how<std::vector<double>>(hhows, "stopelA"))
  {
    stops = std::move(*val);
    if (stops.size() != nrays)
      throw std::runtime_error{"Size mismatch on ray stop elevations"};
  }

  auto angles = std::vector<float>(nrays);
  if (!starts.empty() && !stops.empty())
  {
    for (size_t i = 0; i < nrays; ++i)
      angles[i] = angle_midpoint(starts[i], stops[i]);
  }
  else if (!starts.empty() && nrays > 1)
  {
    for (size_t i = 0; i < nrays - 1; ++i)
      angles[i] = angle_midpoint(starts[i], starts[i+1]);
    angles[nrays-1] = angle_midpoint(starts[nrays-1], starts[nrays-1] + (starts[nrays-1] - starts[nrays-2]));
  }
  else if (!stops.empty() && nrays > 1)
  {
    angles[0] = angle_midpoint(stops[0] - (stops[1] - stops[0]), stops[0]);
    for (size_t i = 1; i < nrays; ++i)
      angles[i] = angle_midpoint(stops[i-1], stops[i]);
  }
  else
  {
    angles.assign(nrays, elangle);
  }

  reorder_rays_from_odim(a1gate, clockwise, angles);

  return angles;
}

static auto extract_a1gate(std::vector<float> const& azimuths) -> std::tuple<size_t, bool, double>
{
  if (azimuths.size() < 2)
    throw std::runtime_error{"Unable to determine a1gate and clockwise (insufficient rays)"};

  // find the ray spanning 0 degrees
  auto iclosest = 0;
  auto diff_closest = std::fabs(azimuths[0] < 180.0f ? azimuths[0] : azimuths[0] - 360.0f);
  for (size_t i = 1; i < azimuths.size(); ++i)
  {
    auto diff = std::fabs(azimuths[i] < 180.0f ? azimuths[i] : azimuths[i] - 360.0f);
    if (diff < diff_closest)
    {
      iclosest = i;
      diff_closest = diff;
    }
  }

  // find direction of rotation
  auto a0 = azimuths[0] < 180.0f ? azimuths[0] : azimuths[0] - 360.0f;
  auto a1 = azimuths[1] < 180.0f ? azimuths[1] : azimuths[1] - 360.0f;
  auto clockwise = a0 < a1;

  // determine astart angle
  auto astart = azimuths[iclosest] - std::fabs(azimuths[(iclosest+1) % azimuths.size()] - azimuths[iclosest]) * 0.5;

  return std::make_tuple(azimuths.size() - iclosest, clockwise, astart);
}

static auto build_bin_ranges(size_t nbins, float rstart, float rscale) -> std::vector<float>
{
  rstart *= 1000.0f;
  rstart += rscale * 0.5f;
  auto ranges = std::vector<float>(nbins);
  for (size_t i = 0; i < nbins; ++i)
    ranges[i] = rstart + i * rscale;
  return ranges;
}

static auto extract_rstart_rscale(std::vector<float> const& ranges) -> std::pair<double, double>
{
  if (ranges.size() < 2)
    throw std::runtime_error{"Unable to determine rstart and rscale (insufficient bins)"};
  auto rscale = ranges[1] - ranges[0];
  auto rstart = (ranges[0] - (rscale * 0.5f)) / 1000.0;
  for (size_t i = 2; i < ranges.size(); ++i)
    if (std::fabs((ranges[i] - ranges[i-1]) - rscale) > 0.001)
      throw std::runtime_error{"Non-uniform range bins not supported by ODIM"};
  return std::make_pair(rstart, rscale);
}

struct odim_source
{
  std::string wigos;
  std::string wmo;
  std::string rad;
  std::string plc;
  std::string nod;
  std::string org;
  std::string cty;
  std::string cmt;
};

static auto parse_odim_source_string(std::string const& source) -> odim_source
{
  odim_source ret;
  auto ipos = size_t(0);
  while (ipos < source.size())
  {
    auto imid = source.find(':', ipos);
    if (imid == std::string::npos)
      throw std::runtime_error{"Failed to parse source string"};
    auto iend = source.find(',', imid);
    if (iend == std::string::npos)
      iend = source.size();

    auto key = std::string_view{source}.substr(ipos, imid - ipos);
    auto val = std::string_view{source}.substr(imid + 1, iend - (imid + 1));

    if (key == "WIGOS")
      ret.wigos = val;
    else if (key == "WMO")
      ret.wmo = val;
    else if (key == "RAD")
      ret.rad = val;
    else if (key == "PLC")
      ret.plc = val;
    else if (key == "NOD")
      ret.nod = val;
    else if (key == "ORG")
      ret.org = val;
    else if (key == "CTY")
      ret.cty = val;
    else if (key == "CMT")
      ret.cmt = val;

    ipos = iend + 1;
  }
  return ret;
}

static auto format_odim_source_string(odim_source const& val) -> std::string
{
  std::string ret;
  if (!val.wigos.empty())
    ret.append("WIGOS:").append(val.wigos).append(",");
  if (!val.wmo.empty())
    ret.append("WMO:").append(val.wmo).append(",");
  if (!val.rad.empty())
    ret.append("RAD:").append(val.rad).append(",");
  if (!val.plc.empty())
    ret.append("PLC:").append(val.plc).append(",");
  if (!val.nod.empty())
    ret.append("NOD:").append(val.nod).append(",");
  if (!val.org.empty())
    ret.append("ORG:").append(val.org).append(",");
  if (!val.cty.empty())
    ret.append("CTY:").append(val.cty).append(",");
  if (!val.cmt.empty())
    ret.append("CMT:").append(val.cmt).append(",");
  if (!ret.empty())
    ret.pop_back();
  return ret;
}

static auto convert_dataset_from_odim(
      hid_t hdataset
    , int64_t a1gate
    , bool clockwise
    , std::vector<std::string> const* qualified_variables // list of variables qualified by this dataset
    , fm301::sweep& sweep
    ) -> std::string
{
  auto hwhat = hdf_group_open(hdataset, "what");
  auto quantity = hdf_attribute_read<std::string>(hwhat, "quantity");
  auto gain = hdf_attribute_read<double>(hwhat, "gain");
  auto offset = hdf_attribute_read<double>(hwhat, "offset");
  auto nodata = hdf_attribute_read<double>(hwhat, "nodata");
  auto undetect = hdf_attribute_read<double>(hwhat, "undetect");

  auto hdata = hdf_dataset_open(hdataset, "data");
  auto type = hdf_dataset_type(hdata);
  auto size = hdf_dataset_size(hdata);

  if (size != sweep.ray_count() * sweep.bin_count())
    throw std::runtime_error{"Dataset size does not match sweep definition"};

  auto qtraits = odim_quantity_traits(quantity);

  // if the default name has already been used append a number until a free one is found
  auto name = qtraits ? qtraits->name : quantity;
  for (auto ivers = 1; sweep.dataset_exists(name); ++ivers)
    name = (qtraits ? qtraits->name : quantity) + std::to_string(ivers);

  auto dataset = sweep.dataset_add(name, type, 5); // TODO - user set compression level
  if (qtraits)
  {
    if (qtraits->standard_name)
      dataset.set_standard_name(qtraits->standard_name);
    if (qtraits->long_name)
      dataset.set_long_name(qtraits->long_name);
    if (qtraits->units)
      dataset.set_units(qtraits->units);
  }

  dataset.set_scale_factor(gain);
  dataset.set_add_offset(offset);
  if (qualified_variables)
  {
    dataset.set_is_quality_field(true);
    dataset.set_qualified_variables(*qualified_variables);
  }

  auto copy_data = [&](auto dummy)
  {
    using T = decltype(dummy);
    dataset.set_fill_value(T(nodata));
    if (T(nodata) != T(undetect))
      dataset.set_undetect(T(undetect));
    // TODO - flag_values and flag_masks
    auto buf = std::vector<T>(size);
    if (H5Dread(hdata, hdf_native_type<T>(), H5S_ALL, H5S_ALL, H5P_DEFAULT, buf.data()) < 0)
      throw std::runtime_error{"Failed to read dataset"};
    reorder_rays_from_odim(a1gate, clockwise, sweep.bin_count(), buf);
    dataset.write_raw(buf);
  };
  switch (type)
  {
  case data_type::i8: copy_data(int8_t(0)); break;
  case data_type::u8: copy_data(uint8_t(0)); break;
  case data_type::i16: copy_data(int16_t(0)); break;
  case data_type::u16: copy_data(uint16_t(0)); break;
  case data_type::i32: copy_data(int32_t(0)); break;
  case data_type::u32: copy_data(uint32_t(0)); break;
  case data_type::i64: copy_data(int64_t(0)); break;
  case data_type::u64: copy_data(uint64_t(0)); break;
  case data_type::f32: copy_data(float(0)); break;
  case data_type::f64: copy_data(double(0)); break;
  }

  auto data_names = std::vector<std::string>{name};
  auto quality_count = indexed_group_count(hdataset, "quality");
  auto quality_names = std::vector<std::string>(quality_count);
  for (size_t iquality = 0; iquality < quality_count; ++iquality)
  {
    auto hquality = hdf_group_open(hdataset, ("quality" + std::to_string(iquality + 1)).c_str());
    quality_names[iquality] = convert_dataset_from_odim(hquality, a1gate, clockwise, &data_names, sweep);
  }
  if (quality_count > 0)
    dataset.set_ancillary_variables(quality_names);

  return name;
}

static auto convert_sweep_from_odim(
      hid_t hsweep
    , std::vector<hdf_handle>& hhows
    , std::chrono::system_clock::time_point& time_coverage_start
    , std::chrono::system_clock::time_point& time_coverage_end
    , fm301::volume& volume
    ) -> void
{
  auto hwhat = hdf_group_open(hsweep, "what");
  auto hwhere = hdf_group_open(hsweep, "where");
  hhows.push_back(hdf_group_exists(hsweep, "how") ? hdf_group_open(hsweep, "how") : hdf_handle{});

  auto start_time = parse_time_strings(
        hdf_attribute_read<std::string>(hwhat, "startdate")
      , hdf_attribute_read<std::string>(hwhat, "starttime"));
  time_coverage_start = std::min(time_coverage_start, start_time);
  auto end_time = parse_time_strings(
        hdf_attribute_read<std::string>(hwhat, "enddate")
      , hdf_attribute_read<std::string>(hwhat, "endtime"));
  time_coverage_end = std::max(time_coverage_end, end_time);
  auto elangle = hdf_attribute_read<double>(hwhere, "elangle");
  auto nbins = hdf_attribute_read<int64_t>(hwhere, "nbins");
  auto nrays = hdf_attribute_read<int64_t>(hwhere, "nrays");
  auto a1gate = hdf_attribute_read<int64_t>(hwhere, "a1gate");
  auto rstart = hdf_attribute_read<double>(hwhere, "rstart");
  auto rscale = hdf_attribute_read<double>(hwhere, "rscale");
  auto ray_angle_resolution = std::numeric_limits<float>::quiet_NaN();
  auto clockwise = true;
  if (auto val = hdf_read_how<double>(hhows, "antspeed"))
    clockwise = *val >= 0.0;
  else if (auto val = hdf_read_how<double>(hhows, "rpm"))
    clockwise = *val >= 0.0;

  auto sweep = volume.sweep_add(nrays, nbins, 1, 0);

  sweep.ray_set_time(build_ray_times(nrays, a1gate, clockwise, start_time, end_time, hhows), start_time);
  sweep.ray_set_azimuth(build_ray_azimuths(nrays, a1gate, clockwise, hhows, ray_angle_resolution));
  sweep.ray_set_elevation(build_ray_elevations(nrays, a1gate, clockwise, hhows, elangle));
  // copy these attributes to all rays because ODIM provides only per-sweep values, but fm301 provides only per-ray
  // note that the calib_index is set later on when processing radar calibrations group
  if (auto val = hdf_read_how<double>(hhows, "NI"))
    sweep.ray_set_nyquist_velocity(std::vector<float>(nrays, *val));
  if (auto val = hdf_read_how<int64_t>(hhows, "Vsamples"))
    sweep.ray_set_n_samples(std::vector<int>(nrays, *val));
  // prfs/prts are a particularly problematic conversion without per-ray information from ODIM
  auto highprf = hdf_read_how<double>(hhows, "highprf");
  auto midprf = hdf_read_how<double>(hhows, "midprf");
  auto lowprf = hdf_read_how<double>(hhows, "lowprf");
  if (midprf)
    throw std::runtime_error{"Triple PRT mode conversion not implemented"};
  else if (highprf && lowprf)
  {
    sweep.ray_set_prt(std::vector<float>(nrays, 1.0 / *highprf));
    sweep.ray_set_prt_ratio(std::vector<float>(nrays, (1.0 / *highprf) / (1.0 / *lowprf)));
    sweep.set_prt_mode("dual");
  }
  else if (highprf || lowprf)
  {
    sweep.ray_set_prt(std::vector<float>(nrays, 1.0 / (highprf ? *highprf : *lowprf)));
    sweep.set_prt_mode("fixed");
  }

  sweep.bin_set_range(build_bin_ranges(nbins, rstart, rscale));

  if (auto val = hdf_read_how<double>(hhows, "wavelength"))
  {
    float frequency = speed_of_light / (*val * 0.01); // wavelength in cm
    sweep.frequency_set_frequency(std::vector<float>(1, frequency));
  }

  if (auto val = hdf_read_how<int64_t>(hhows, "scan_index"))
    sweep.set_sweep_number(*val - 1); // convert from 1-based to 0-based
  bool hpol = true, vpol = true;
  if (auto val = hdf_read_how<std::string>(hhows, "polmode"))
  {
    if (*val == "single-H")
    {
      sweep.set_polarization_mode("horizontal");
      vpol = false;
    }
    else if (*val == "single-V")
    {
      sweep.set_polarization_mode("vertical");
      hpol = false;
    }
    else if (*val == "simultaneous-dual")
      sweep.set_polarization_mode("hv_sim");
    else if (*val == "switched-dual")
      sweep.set_polarization_mode("hv_alt");
  }
  sweep.set_fixed_angle(elangle);
  if (!std::isnan(ray_angle_resolution))
  {
    sweep.set_rays_are_indexed(true);
    sweep.set_ray_angle_resolution(ray_angle_resolution);
  }
  if (auto val = hdf_read_how<double>(hhows, "antspeed"))
    sweep.set_target_scan_rate(*val);

  auto monitoring = sweep.monitoring_add();
  if (auto val = hdf_read_how<std::vector<float>>(hhows, "TXpower"))
  {
    reorder_rays_from_odim(a1gate, clockwise, *val);
    if (hpol)
      monitoring.set_measured_transmit_power_h(*val);
    if (vpol)
      monitoring.set_measured_transmit_power_v(*val);
  }

  auto data_count = indexed_group_count(hsweep, "data");
  auto data_names = std::vector<std::string>(data_count);
  for (size_t idata = 0; idata < data_count; ++idata)
  {
    auto hdata = hdf_group_open(hsweep, ("data" + std::to_string(idata + 1)).c_str());
    data_names[idata] = convert_dataset_from_odim(hdata, a1gate, clockwise, nullptr, sweep);
  }

  auto quality_count = indexed_group_count(hsweep, "quality");
  auto quality_names = std::vector<std::string>(quality_count);
  for (size_t iquality = 0; iquality < quality_count; ++iquality)
  {
    auto hquality = hdf_group_open(hsweep, ("quality" + std::to_string(iquality + 1)).c_str());
    quality_names[iquality] = convert_dataset_from_odim(hquality, a1gate, clockwise, &data_names, sweep);
  }
  if (quality_count > 0)
  {
    for (auto& name : data_names)
    {
      auto dataset = sweep.dataset(name);
      if (auto existing = dataset->ancillary_variables())
      {
        existing->insert(existing->end(), quality_names.begin(), quality_names.end());
        dataset->set_ancillary_variables(*existing);
      }
      else
        dataset->set_ancillary_variables(quality_names);
    }
  }

  hhows.pop_back();
}

auto convert_radar_parameters_from_odim(std::vector<hdf_handle>& hhows, volume& vol)
{
  auto radar_params = vol.radar_parameters_add();
  if (auto val = hdf_read_how<double>(hhows, "beamwidth"))
  {
    radar_params.set_beam_width_h(*val);
    radar_params.set_beam_width_v(*val);
  }
  if (auto val = hdf_read_how<double>(hhows, "beamwH"))
    radar_params.set_beam_width_h(*val);
  if (auto val = hdf_read_how<double>(hhows, "beamwV"))
    radar_params.set_beam_width_v(*val);
  if (auto val = hdf_read_how<double>(hhows, "antgainH"))
    radar_params.set_antenna_gain_h(*val);
  if (auto val = hdf_read_how<double>(hhows, "antgainV"))
    radar_params.set_antenna_gain_v(*val);
  if (auto val = hdf_read_how<double>(hhows, "RXbandwidth"))
    radar_params.set_receiver_bandwidth(*val * 1000000.0); // convert MHz to hz
}

auto convert_radar_calibrations(std::vector<hdf_handle> const& hdatasets, hdf_handle hhow_vol, volume& vol)
{
  struct pwinfo
  {
    double                  pulsewidth; // odim pulsewidth value
    std::vector<size_t>     sweeps;     // index of sweeps that will use this calibration
    std::vector<hdf_handle> hhows;      // how groups for volume, and all scans using this pulsewidth
    pwinfo(double pulsewidth, hdf_handle hhow) : pulsewidth{pulsewidth}, hhows{std::move(hhow)} { }
  };
  auto pws = std::vector<pwinfo>();

  // do we have a volume wide pulsewidth?
  if (hhow_vol && hdf_attribute_exists(hhow_vol, "pulsewidth"))
    pws.emplace_back(hdf_attribute_read<double>(hhow_vol, "pulsewidth"), hhow_vol);
  else
    pws.emplace_back(std::numeric_limits<double>::quiet_NaN(), hhow_vol);

  // collect a list of unique pulsewidths used by sweeps
  for (size_t isweep = 0; isweep < hdatasets.size(); ++isweep)
  {
    size_t icalib = 0;
    if (hdf_group_exists(hdatasets[isweep], "how"))
    {
      auto hhow = hdf_group_open(hdatasets[isweep], "how");
      if (hdf_attribute_exists(hhow, "pulsewidth"))
      {
        auto val = hdf_attribute_read<double>(hhow, "pulsewidth");
        icalib = std::find_if(pws.begin(), pws.end(), [&](auto pw){ return std::fabs(pw.pulsewidth - val) < 0.001; }) - pws.begin();
        if (icalib == pws.size())
          pws.emplace_back(val, hhow_vol);
      }
      pws[icalib].hhows.push_back(std::move(hhow));
    }
    pws[icalib].sweeps.push_back(isweep);
  }

  // if no sweeps are using the first calib that we created as a catch all then eliminate it
  if (pws[0].sweeps.empty())
    pws.erase(pws.begin());

  // covertt calibration metadata for each detected pulsewidth
  auto calib = vol.radar_calibration_add(pws.size());
  for (size_t i = 0; i < pws.size(); ++i)
  {
    // detect and set metadata in the radar_calibration group
    calib.set_calib_index(i, i);
    if (!std::isnan(pws[i].pulsewidth))
      calib.set_pulse_width(i, pws[i].pulsewidth * 0.000001); // us to s
    if (auto val = hdf_read_how<double>(pws[i].hhows, "antgainH"))
      calib.set_antenna_gain_h(i, *val);
    if (auto val = hdf_read_how<double>(pws[i].hhows, "antgainV"))
      calib.set_antenna_gain_v(i, *val);
    if (auto val = hdf_read_how<double>(pws[i].hhows, "radomelossH"))
      calib.set_two_way_radome_loss_h(i, *val * 2.0); // one way to two way
    if (auto val = hdf_read_how<double>(pws[i].hhows, "radomelossV"))
      calib.set_two_way_radome_loss_v(i, *val * 2.0); // one way to two way
    if (auto val = hdf_read_how<double>(pws[i].hhows, "radconstH"))
      calib.set_radar_constant_h(i, *val);
    if (auto val = hdf_read_how<double>(pws[i].hhows, "radconstV"))
      calib.set_radar_constant_v(i, *val);
    if (auto val = hdf_read_how<double>(pws[i].hhows, "nsampleH"))
      calib.set_noise_hc(i, *val);
    if (auto val = hdf_read_how<double>(pws[i].hhows, "nsampleV"))
      calib.set_noise_vc(i, *val);
    if (auto val = hdf_read_how<double>(pws[i].hhows, "NEZH"))
      calib.set_base_1km_hc(i, *val);
    if (auto val = hdf_read_how<double>(pws[i].hhows, "NEZV"))
      calib.set_base_1km_vc(i, *val);
    if (auto val = hdf_read_how<double>(pws[i].hhows, "zdrcal"))
      calib.set_zdr_correction(i, *val);
    if (auto val = hdf_read_how<double>(pws[i].hhows, "phasediff"))
      calib.set_system_phidp(i, *val);

    // set calib index for all rays in each sweep associated with this calibration
    for (auto isweep : pws[i].sweeps)
    {
      auto sweep = vol.sweep(isweep);
      sweep.ray_set_calib_index(std::vector<int>(sweep.ray_count(), i));
    }
  }
}

auto convert_from_odim(
      std::filesystem::path const& path_in
    , std::filesystem::path const& path_out
    , std::string const& cmdline
    ) -> void
{
  auto hvolume = hdf_handle{H5Fopen(path_in.c_str(), H5F_ACC_RDONLY, H5P_DEFAULT)};
  if (!hvolume)
    throw std::runtime_error{"Failed to open HDF5 file " + path_in.native()};
  auto hwhat = hdf_group_open(hvolume, "what");
  auto hwhere = hdf_group_open(hvolume, "where");
  auto hhows = std::vector<hdf_handle>();
  hhows.push_back(hdf_group_exists(hvolume, "how") ? hdf_group_open(hvolume, "how") : hdf_handle{});

  auto source = parse_odim_source_string(hdf_attribute_read<std::string>(hwhat, "source"));

  auto vol = volume{path_out, io_mode::create};
  {
    // output history using same format as the NCO tools
    auto tt = std::time(0);
    auto tm = std::gmtime(&tt);
    char tstr[32];
    strftime(tstr, 32, "%a %b %m %H:%M:%S %Y: ", tm);
    vol.set_history(tstr + cmdline);
  }
  if (auto val = hdf_read_how<std::string>(hhows, "comment"))
    vol.set_comment(*val);
  if (!source.rad.empty())
    vol.set_instrument_name(source.rad);
  if (!source.plc.empty())
    vol.set_site_name(source.plc);
  if (auto val = hdf_read_how<std::string>(hhows, "task"))
    vol.set_scan_name(*val);
  vol.set_platform_is_mobile(false);
  if (auto val = hdf_read_how<std::string>(hhows, "simulated"))
    vol.set_simulated_data(*val == "True");
  else
    vol.set_simulated_data(false);
  vol.set_wmo_cf_profile("FM 301-XX");
  vol.set_wmo_data_category(6); // C-13 code for radar data
  if (!source.org.empty())
    vol.set_wmo_originating_centre(std::stoi(source.org));
  if (!source.wmo.empty())
    vol.set_wmo_id(source.wmo);
  if (!source.wigos.empty())
    vol.set_wmo_wsi(source.wigos);
  vol.set_platform_type("fixed");
  vol.set_instrument_type("radar");
  vol.set_primary_axis("axis_z");
  vol.set_latitude(hdf_attribute_read<double>(hwhere, "lat"));
  vol.set_longitude(hdf_attribute_read<double>(hwhere, "lon"));
  vol.set_altitude(hdf_attribute_read<double>(hwhere, "height"));
  if (auto val = hdf_read_how<std::string>(hhows, "radar_msg"))
    vol.set_status_str(*val);

  convert_radar_parameters_from_odim(hhows, vol);

  // cache the sweep object handles since we need to loop through them a couple of times
  auto sweep_count = indexed_group_count(hvolume, "dataset");
  auto hdatasets = std::vector<hdf_handle>(sweep_count);
  for (size_t isweep = 0; isweep < sweep_count; ++isweep)
    hdatasets[isweep] = hdf_group_open(hvolume, ("dataset" + std::to_string(isweep + 1)).c_str());

  // convert the actual sweeps
  auto time_coverage_start = std::chrono::system_clock::time_point::max();
  auto time_coverage_end = std::chrono::system_clock::time_point::min();
  for (size_t isweep = 0; isweep < sweep_count; ++isweep)
    convert_sweep_from_odim(hdatasets[isweep], hhows, time_coverage_start, time_coverage_end, vol);
  vol.set_time_coverage_start(time_coverage_start);
  vol.set_time_coverage_end(time_coverage_end);

  // collect and build radar calibrations from metadata scattered throughout the sweeps
  // must be called after sweep conversion (uses sweep objects)
  convert_radar_calibrations(hdatasets, hhows[0], vol);
}

auto convert_dataset_to_odim(sweep const& sweep, size_t idataset, size_t a1gate, bool clockwise, hid_t hsweep) -> void
{
  auto dataset = *sweep.dataset(idataset);

  char grp_name[32];
  snprintf(grp_name, 32, "data%lu", idataset + 1);
  auto hdata = hdf_group_create(hsweep, grp_name);

  auto hwhat = hdf_group_create(hdata, "what");
  hdf_attribute_create(hwhat, "quantity", odim_quantity_from_varname(dataset.name()));
  if (auto val = dataset.scale_factor())
    hdf_attribute_create(hwhat, "gain", *val);
  if (auto val = dataset.add_offset())
    hdf_attribute_create(hwhat, "offset", *val);

  auto type = dataset.type();
  hsize_t dimlens[2] = { sweep.ray_count(), sweep.bin_count() };
  auto space = hdf_handle{H5Screate_simple(2, dimlens, dimlens)};
  if (!space)
    throw std::runtime_error{"Failed to create dataspace"};
  auto plist = hdf_handle{H5Pcreate(H5P_DATASET_CREATE)};
  if (!plist)
    throw std::runtime_error{"Failed to create property list"};
  auto compression = 5; // TODO - user set compression level
  if (H5Pset_chunk(plist, 2, dimlens) < 0 || (compression > 0 && H5Pset_deflate(plist, compression) < 0))
    throw std::runtime_error{"Failed to create property list"};
  auto hvar = hdf_handle{H5Dcreate(hdata, "data", hdf_storage_type(type), space, H5P_DEFAULT, plist, H5P_DEFAULT)};
  if (!hvar)
    throw std::runtime_error{"Failed to create dataset"};
  hdf_attribute_create(hvar, "CLASS", "IMAGE");
  hdf_attribute_create(hvar, "IMAGE_VERSION", "1.2");

  auto copy_data = [&](auto dummy)
  {
    using T = decltype(dummy);
    if (auto val = dataset.fill_value())
      hdf_attribute_create(hwhat, "nodata", double(std::get<T>(*val)));
    if (auto val = dataset.undetect())
      hdf_attribute_create(hwhat, "undetect", double(std::get<T>(*val)));
    else if (auto val = dataset.fill_value())
      hdf_attribute_create(hwhat, "undetect", double(std::get<T>(*val)));
    auto buf = std::vector<T>(sweep.ray_count() * sweep.bin_count());
    dataset.read_raw(buf);
    reorder_rays_to_odim(a1gate, clockwise, sweep.bin_count(), buf);
    if (H5Dwrite(hvar, hdf_native_type<T>(), H5S_ALL, H5S_ALL, H5P_DEFAULT, buf.data()) < 0)
      throw std::runtime_error{"Failed to write dataset"};
  };
  switch (type)
  {
  case data_type::i8: copy_data(int8_t(0)); break;
  case data_type::u8: copy_data(uint8_t(0)); break;
  case data_type::i16: copy_data(int16_t(0)); break;
  case data_type::u16: copy_data(uint16_t(0)); break;
  case data_type::i32: copy_data(int32_t(0)); break;
  case data_type::u32: copy_data(uint32_t(0)); break;
  case data_type::i64: copy_data(int64_t(0)); break;
  case data_type::u64: copy_data(uint64_t(0)); break;
  case data_type::f32: copy_data(float(0)); break;
  case data_type::f64: copy_data(double(0)); break;
  }
}

auto is_uniform_value(std::span<int> data) -> bool
{
  if (data.empty())
    return false;
  auto val = data[0];
  for (size_t i = 1; i < data.size(); ++i)
    if (data[i] != val)
      return false;
  return true;
}

auto is_uniform_value(std::span<float> data) -> bool
{
  if (data.empty())
    return false;
  auto val = data[0];
  for (size_t i = 1; i < data.size(); ++i)
    if (std::fabs(data[i] - val) > 0.001f)
      return false;
  return true;
}

auto convert_sweep_to_odim(volume const& vol, size_t isweep, hid_t hvolume) -> void
{
  auto sweep = vol.sweep(isweep);

  char dsname[32];
  snprintf(dsname, 32, "dataset%lu", isweep + 1);
  auto hdataset = hdf_group_create(hvolume, dsname);

  auto hwhat = hdf_group_create(hdataset, "what");
  hdf_attribute_create(hwhat, "product", "SCAN");
  if (auto val = sweep.ray_time())
  {
    auto start_time = val->second + std::chrono::duration_cast<std::chrono::system_clock::duration>(val->first.front());
    auto start_time_strs = format_time_strings(start_time);
    hdf_attribute_create(hwhat, "startdate", start_time_strs.first);
    hdf_attribute_create(hwhat, "starttime", start_time_strs.second);

    auto end_time = val->second + std::chrono::duration_cast<std::chrono::system_clock::duration>(val->first.back());
    auto end_time_strs = format_time_strings(end_time);
    hdf_attribute_create(hwhat, "enddate", end_time_strs.first);
    hdf_attribute_create(hwhat, "endtime", end_time_strs.second);
  }

  auto hwhere = hdf_group_create(hdataset, "where");
  hdf_attribute_create(hwhere, "nrays", long(sweep.ray_count()));
  hdf_attribute_create(hwhere, "nbins", long(sweep.bin_count()));
  if (auto val = sweep.fixed_angle())
    hdf_attribute_create(hwhere, "elangle", *val);
  auto start_scale = extract_rstart_rscale(sweep.bin_range().value());
  hdf_attribute_create(hwhere, "rstart", start_scale.first);
  hdf_attribute_create(hwhere, "rscale", start_scale.second);
  auto [a1gate, clockwise, astart] = extract_a1gate(sweep.ray_azimuth().value());
  hdf_attribute_create(hwhere, "a1gate", long(a1gate));

  auto hhow = hdf_group_create(hdataset, "how");
  hdf_attribute_create(hhow, "astart", astart);
  if (auto val = sweep.sweep_number())
    hdf_attribute_create(hhow, "scan_index", long(*val + 1));
  if (auto val = sweep.polarization_mode())
  {
    if (*val == "horizontal")
      hdf_attribute_create(hhow, "polmode", "single-H");
    else if (*val == "vertical")
      hdf_attribute_create(hhow, "polmode", "single-V");
    else if (*val == "hv_sim")
      hdf_attribute_create(hhow, "polmode", "simultaneous-dual");
    else if (*val == "hv_alt")
      hdf_attribute_create(hhow, "polmode", "switched-dual");
  }
  if (auto val = sweep.target_scan_rate())
    hdf_attribute_create(hhow, "antspeed", *val);
  if (auto val = sweep.ray_nyquist_velocity())
  {
    if (is_uniform_value(*val))
      hdf_attribute_create(hhow, "NI", (*val)[0]);
    else
      std::cout << "Unable to convert per-ray nyquist to single value" << std::endl;
  }
  if (auto val = sweep.ray_n_samples())
  {
    if (is_uniform_value(*val))
      hdf_attribute_create(hhow, "Vsamples", long((*val)[0]));
    else
      std::cout << "Unable to convert per-ray samples to single value" << std::endl;
  }
  if (auto val = sweep.ray_prt())
  {
    if (is_uniform_value(*val))
    {
      auto prf = 1.0 / (*val)[0];
      if (auto val2 = sweep.ray_prt_ratio())
      {
        if (is_uniform_value(*val2))
        {
          auto lowprf = prf * (*val2)[0];
          if (prf < lowprf)
            std::swap(prf, lowprf);
          hdf_attribute_create(hhow, "lowprf", lowprf);
        }
        else
          std::cout << "Unable to convert per-ray prf ratio to single value" << std::endl;
      }
      hdf_attribute_create(hhow, "highprf", prf);
    }
    else
      std::cout << "Unable to convert per-ray prfs to single value" << std::endl;
  }
  // TODO ray specific startelA, stopelA, startT, stopT (can't really convert these from cfradia2's 'midpoints' though)
  if (auto monitoring = sweep.monitoring())
  {
    if (auto val = monitoring->measured_transmit_power_h())
    {
      reorder_rays_to_odim(a1gate, clockwise, *val);
      hdf_attribute_create(hhow, "TXpower", *val);
    }
    else if (auto val = monitoring->measured_transmit_power_v())
    {
      reorder_rays_to_odim(a1gate, clockwise, *val);
      hdf_attribute_create(hhow, "TXpower", *val);
    }
  }
  if (sweep.frequency_count() == 1)
  {
    if (auto val = sweep.frequency_frequency())
    {
      auto wavelength = speed_of_light / ((*val)[0] * 0.01);
      hdf_attribute_create(hhow, "wavelength", wavelength);
    }
  }
  else if (sweep.frequency_count() > 1)
    std::cout << "Unable to convert multi-frequency sweep metadata" << std::endl;

  for (size_t i = 0; i < sweep.dataset_count(); ++i)
    convert_dataset_to_odim(sweep, i, a1gate, clockwise, hdataset);
}

auto convert_to_odim(
      std::filesystem::path const& path_in
    , std::filesystem::path const& path_out
    ) -> void
{
  auto vol = volume{path_in, io_mode::read_only};

  auto hvolume = hdf_handle{H5Fcreate(path_out.c_str(), H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT)};
  if (!hvolume)
    throw std::runtime_error{"Failed to create HDF5 file " + path_out.native()};
  hdf_attribute_create(hvolume, "Conventions", "ODIM_H5/V2_3");

  auto hwhat = hdf_group_create(hvolume, "what");
  hdf_attribute_create(hwhat, "object", "PVOL");
  hdf_attribute_create(hwhat, "version", "H5rad 2.3");
  auto source = odim_source{};
  if (auto val = vol.instrument_name())
    source.rad = *val;
  if (auto val = vol.site_name())
    source.plc = *val;
  if (auto val = vol.wmo_originating_centre())
    source.org = std::to_string(*val);
  if (auto val = vol.wmo_id())
    source.wmo = *val;
  if (auto val = vol.wmo_wsi())
    source.wigos = *val;
  hdf_attribute_create(hwhat, "source", format_odim_source_string(source));
  if (auto val = vol.time_coverage_start())
  {
    auto date_time_str = format_time_strings(*val);
    hdf_attribute_create(hwhat, "date", date_time_str.first);
    hdf_attribute_create(hwhat, "time", date_time_str.second);
  }

  auto hwhere = hdf_group_create(hvolume, "where");
  if (auto val = vol.latitude())
    hdf_attribute_create(hwhere, "lat", *val);
  if (auto val = vol.longitude())
    hdf_attribute_create(hwhere, "lon", *val);
  if (auto val = vol.altitude())
    hdf_attribute_create(hwhere, "height", *val);

  auto hhow = hdf_group_create(hvolume, "how");
  if (auto val = vol.comment())
    hdf_attribute_create(hhow, "comment", *val);
  if (auto val = vol.scan_name())
    hdf_attribute_create(hhow, "task", *val);
  if (auto val = vol.simulated_data())
    hdf_attribute_create(hhow, "simulated", *val);
  if (auto val = vol.status_str())
    hdf_attribute_create(hhow, "radar_msg", *val);
  if (auto params = vol.radar_parameters())
  {
    if (auto val = params->antenna_gain_h())
      hdf_attribute_create(hhow, "antgainH", *val);
    if (auto val = params->antenna_gain_v())
      hdf_attribute_create(hhow, "antgainV", *val);
    if (auto val = params->beam_width_h())
      hdf_attribute_create(hhow, "beamwH", *val);
    if (auto val = params->beam_width_v())
      hdf_attribute_create(hhow, "beamwV", *val);
    if (auto val = params->receiver_bandwidth())
      hdf_attribute_create(hhow, "RXbandwidth", (*val) / 1000000.0); // convert hz to MHz
  }
  if (auto calib = vol.radar_calibration())
  {
    // TODO - convert calibrations
  }

  for (size_t isweep = 0; isweep < vol.sweep_count(); ++isweep)
    convert_sweep_to_odim(vol, isweep, hvolume);
}
