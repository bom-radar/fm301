/*---------------------------------------------------------------------------------------------------------------------
 * FM301 C++ API and Toolkit
 *
 * Copyright 2021 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *-------------------------------------------------------------------------------------------------------------------*/
#include "fm301.h"
#include <getopt.h>
#include <cstdlib>
#include <iostream>

using namespace fm301;

// forward declare conversion functions from our convert-xxx.cc files
auto convert_from_odim(
      std::filesystem::path const& path_in
    , std::filesystem::path const& path_out
    , std::string const& cmdline
    ) -> void;
auto convert_to_odim(
      std::filesystem::path const& path_in
    , std::filesystem::path const& path_out
    ) -> void;

enum class file_format
{
    unknown
  , fm301
  , odim_h5
};

static auto parse_file_format(std::string_view const& str)
{
  if (str == "fm301")
    return file_format::fm301;
  if (str == "odim_h5")
    return file_format::odim_h5;
  throw std::runtime_error{std::string("Invalid format ").append(str)};
}

static auto determine_file_format(std::filesystem::path const& path)
{
  auto extension = path.extension();
  if (extension == ".nc")
    return file_format::fm301;
  if (extension == ".h5")
    return file_format::odim_h5;
  return file_format::unknown;
}

constexpr auto usage_string =
R"(FM301 Conversion Utility

Usage:
  fm301-convert [options] input_file output_file

Available Options:
  -h, --help
      Show this message and exit

  --version
      Show version information and exit

  --format-in=(fm301|odim_h5)
      Specify input format rather than relying on automatic detection

  --format-out=(fm301|odim_h5)
      Specify output format rather than relying on automatic detection
)";

const char* short_options = "h";
struct option long_options[] =
{
    { "help",           no_argument,       0, 'h' }
  , { "version",        no_argument,       0, 'V' }
  , { "format-in",      required_argument, 0, 'I' }
  , { "format-out",     required_argument, 0, 'O' }
  , { 0, 0, 0, 0 }
};

int main(int argc, char* argv[])
try
{
  auto format_in = file_format::unknown;
  auto format_out = file_format::unknown;
  while (true)
  {
    int option_index = 0;
    int c = getopt_long(argc, argv, short_options, long_options, &option_index);
    if (c == -1)
      break;
    switch (c)
    {
    case 'h':
      std::cout << usage_string;
      return EXIT_SUCCESS;
    case 'V':
      throw std::logic_error{"Unimplemented"};
      return EXIT_SUCCESS;
    case 'I':
      format_in = parse_file_format(optarg);
      break;
    case 'O':
      format_out = parse_file_format(optarg);
      break;
    case '?':
      std::cerr << "Invalid option\nTry --help for usage instructions" << std::endl;
      return EXIT_FAILURE;
    }
  }

  if (argc - optind != 2)
  {
    std::cerr << "Invalid parameter\nTry --help for usage instructions" << std::endl;
    return EXIT_FAILURE;
  }

  auto path_in = std::filesystem::path{argv[optind]};
  auto path_out = std::filesystem::path{argv[optind+1]};

  // record the command line for use in history attribute
  auto cmdline = std::string(argv[0]);
  for (auto i = 1; i < argc; ++i)
    cmdline.append(" ").append(argv[i]);

  // determine input and output formats (if not explicitly set by user)
  if (format_in == file_format::unknown)
  {
    format_in = determine_file_format(path_in);
    if (format_in == file_format::unknown)
    {
      std::cerr << "Failed to determine format of input file, use --format-in" << std::endl;
      return EXIT_FAILURE;
    }
  }
  if (format_out == file_format::unknown)
  {
    format_out = determine_file_format(path_out);
    if (format_out == file_format::unknown)
    {
      std::cerr << "Failed to determine format of output file, use --format-out" << std::endl;
      return EXIT_FAILURE;
    }
  }

  // run the conversion
  if (false) { }
#if FM301_ENABLE_ODIM
  else if (format_in == file_format::odim_h5 && format_out == file_format::fm301)
    convert_from_odim(path_in, path_out, cmdline);
  else if (format_in == file_format::fm301 && format_out == file_format::odim_h5)
    convert_to_odim(path_in, path_out);
#endif
  else
  {
    std::cerr << "Invalid or unsupported conversion requested" << std::endl;
    return EXIT_FAILURE;
  }

  return EXIT_SUCCESS;
}
catch (std::exception& err)
{
  std::cerr << "Fatal error: " << err.what() << std::endl;
  return EXIT_FAILURE;
}
